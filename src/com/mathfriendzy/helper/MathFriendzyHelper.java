package com.mathfriendzy.helper;


import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mathfriendzy.controller.ads.AdsResponseFileParser;
import com.mathfriendzy.controller.ads.CustomeHouseAdsData;
import com.mathfriendzy.controller.ads.OnAdsDataFromServerSuccess;
import com.mathfriendzy.controller.base.MyApplication;
import com.mathfriendzy.controller.chooseavtar.ChooseAvtars;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.login.LoginActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.moreapp.AppDetail;
import com.mathfriendzy.controller.moreapp.MoreAppFileParser;
import com.mathfriendzy.controller.moreapp.MoreAppListener;
import com.mathfriendzy.controller.registration.RegistrationStep1;
import com.mathfriendzy.controller.resources.ActResourceHome;
import com.mathfriendzy.controller.resources.GetKhanVideoLinkParam;
import com.mathfriendzy.controller.resources.GetResourceCategoriesParam;
import com.mathfriendzy.controller.resources.ResourceCategory;
import com.mathfriendzy.controller.resources.ResourceSearchTermParam;
import com.mathfriendzy.controller.school.SearchTeacherActivity;
import com.mathfriendzy.controller.school.SearchYourSchoolActivity;
import com.mathfriendzy.controller.webview.MyWebView;
import com.mathfriendzy.controller.youtubeplayer.ActYouTubePlaye;
import com.mathfriendzy.listener.EditTextFocusChangeListener;
import com.mathfriendzy.listener.LoginRegisterPopUpListener;
import com.mathfriendzy.listener.OnRegistration;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.listener.OnRequestCompleteWithStringResult;
import com.mathfriendzy.model.chooseAvtar.AvtarServerOperation;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.grade.Grade;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.schools.SchoolDTO;
import com.mathfriendzy.model.schools.TeacherDTO;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.newinappclasses.GetAppUnlockStatusParam;
import com.mathfriendzy.newinappclasses.GetAppUnlockStatusResponse;
import com.mathfriendzy.newinappclasses.GetResourceVideoInAppResult;
import com.mathfriendzy.newinappclasses.GetResourceVideoInAppStatusParam;
import com.mathfriendzy.newinappclasses.OnPurchaseDone;
import com.mathfriendzy.newinappclasses.UpdateUserCoinsParam;
import com.mathfriendzy.newinappclasses.YesNoDialogMessagesForInAppPopUp;
import com.mathfriendzy.newinappclasses.YesNoListenerInterface;
import com.mathfriendzy.notification.AddUserWithAndroidDevice;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.AppVersion;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateObj;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.ITextIds;
import com.mathfriendzy.utils.Validation;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.seventhgradepaid.R;

public class MathFriendzyHelper {

	public static final String DEFAULT_PROFILE_IMAGE = "Smiley";
	public static final String GRADE_TEXT = "Select a Grade";
	public static final String ADULT = "Adult";
	public static final String UNITED_STATE = "United States";
	public static final String CANADA = "Canada";
	public static final String HOME_SCHOOL = "Home School";
	public static final int SELECT_SCHOOL_REQUEST_CODE = 11001;
	public static final int SELECT_TEACHER_REQUEST_CODE = 11002;
	public static final int ADULT_GRADE = 13;
	public static final int NO_GRADE_SELETED = -1;
	public static final String US_DEFAULT_STATE = "California";
	public static final String CANADA_DEFAULT_STATE = "Alberta";
	public static final String DEFAULT_US_CITY = "San Diego";
	public static final String DEFAULT_US_ZIP = "92101";
	public static final String DEFAULT_CANADA_CITY = "Corner Brook";
	public static final String DEFAULT_CITY = "Delhi";
	private static ProgressDialog pd = null;
	public static final int MAX_NUMBER_OF_PLAYER = 4;
	public static final String ENG_LANGUAGE_ID = "1";
	public static final String ENG_LANGuAGE_CODE = "EN";
	public static final int TAB_HEIGHT = 752;
	public static final int TAB_WIDTH = 480;
	public static final int TAB_DENISITY = 160;
	public static final int YES = 1;
	public static final int NO = 0;
	private static String MATH_FRENDZY_PREFF = "MATH_FRENDZY_PREFF";
	public static final String SHOW_RATE_US_POP_UP_KEY = "rateUsPopUp";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	//for account type
	public static final int TEACHER = 1;
	public static final int STUDENT = 2;
	public static final int PARENT = 3;

	public static final String HOUSE_ADS_FREQUENCY_KEY = "HOUSE_ADS_FREQUENCY_KEY";
	public static final String RATE_ADS_FREQUENCY_KEY = "RATE_ADS_FREQUENCY_KEY";
	private static boolean isSubscriptionPurchase = false;

	public static final String HOUSE_ADS_IMAGE_NAME = "House_Ads_Image_Name";
	public static final String HOUSE_ADS_LINK_URL = "House_Ads_Link_Url";
	private static final String DEFAULT_HOUSE_ADS_IMAGE_NAME = "768 x 1024.png";
	private static final String DEFAULT_HOUSE_ADS_LINK_URL = "www.MathTutorPlus.com";

	/**
	 * register user on server
	 *
	 * @param context
	 * @param regObj
	 * @param listener
	 */
	public static void registerUser(final Context context, final RegistereUserDto regObj,
			final OnRegistration listener) {

		new AsyncTask<Void, Void, Void>() {
			private int registreationResult = 0;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... params) {
				Register registerObj = new Register(context);
				registreationResult = registerObj.registerUserOnserver(regObj);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				listener.onRegistrationComplete(registreationResult);
			}
		}.execute();
	}

	/**
	 * Add user on server with the login device
	 *
	 * @param conetx
	 */
	public static void addUserOnServerWithAndroidDevice(Context conetx) {
		UserRegistrationOperation userObj = new UserRegistrationOperation(conetx);
		new AddUserWithAndroidDevice(userObj.getUserId(), conetx).execute(null, null, null);
	}

	/**
	 * Get user id of temp player which is recently added into the user account at the time
	 * of registration
	 *
	 * @param contex
	 * @param tempPlayer
	 * @return
	 */
	public static String getUserIdOfTempPlayerWhichIsNowRegister(Context contex, TempPlayer tempPlayer) {
		UserPlayerOperation userOpr = new UserPlayerOperation(contex);
		String userId = userOpr.getUserIdbyUserName(tempPlayer.getUserName());
		userOpr.closeConn();
		return userId;
	}

	/**
	 * Update temp player which is recently added into user account at the time of registration
	 *
	 * @param userId
	 * @param playerId
	 * @param context
	 */
	public static void updateTempPlayerDataWhichIsNotRegisteredPlayed(String userId,
			String playerId, Context context) {
		LearningCenterimpl learnignCenter = new LearningCenterimpl(context);
		learnignCenter.openConn();
		learnignCenter.updatePlayerTotalPointsForUserIdandPlayerId(userId, playerId);
		learnignCenter.updatePlayerEquationTabelForUserIdAndPlayerId(userId, playerId);
		learnignCenter.updateMathResultForUserIdAndPlayerId(userId, playerId);
		learnignCenter.closeConn();
	}

	/**
	 * Update the player avatar and get avatar list
	 *
	 * @param userId
	 * @param playerId
	 * @param context
	 * @return
	 */
	public static ArrayList<String> updatePlayerAvterAndGetList(String userId, String playerId
			, Context context) {
		ChooseAvtarOpration opr = new ChooseAvtarOpration();
		opr.openConn(context);
		opr.updateplayerAvtarStatusForTempPlayer(userId, playerId);
		ArrayList<String> avtarList = opr.getAvtarIds(userId, playerId);
		opr.closeConn();
		return avtarList;
	}

	/**
	 * Save purchased avatar which is purchased at the time of temp player
	 *
	 * @param userId
	 * @param playerId
	 * @param avtarIdList
	 */
	public static final void saveAvatar(final String userId, final String playerId,
			final ArrayList<String> avtarIdList) {

		new AsyncTask<Void, Void, Void>() {
			private String avtarIds;

			@Override
			protected void onPreExecute() {
				avtarIds = "";
				for (int i = 0; i < avtarIdList.size(); i++) {
					avtarIds = avtarIds + avtarIdList.get(i);
					if (i < avtarIdList.size() - 1)
						avtarIds = avtarIds + ",";
				}
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... params) {
				AvtarServerOperation serverObj = new AvtarServerOperation();
				serverObj.saveAvtar(userId, playerId, avtarIds);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
			}
		}.execute();
	}

	/**
	 * Get the purrchased item list
	 *
	 * @param context
	 * @param userId
	 * @return
	 */
	public static ArrayList<String> getUserPurchaseItemList(Context context, String userId) {
		LearningCenterimpl lrarningImpl = new LearningCenterimpl(context);
		lrarningImpl.openConn();
		lrarningImpl.updatePurchasedItemTable(userId);
		ArrayList<String> purchaseItemList = lrarningImpl.getPurchaseItemIdsByUserId(userId);
		lrarningImpl.closeConn();
		return purchaseItemList;
	}

	/**
	 * Save purchased item on server
	 *
	 * @param context
	 * @param userId
	 * @param purchasedItemList
	 */
	public static void savePurchasedItem(final Context context, final String userId,
			final ArrayList<String> purchasedItemList) {

		new AsyncTask<Void, Void, Void>() {
			private String itemsId;

			@Override
			protected void onPreExecute() {
				itemsId = "";
				for (int i = 0; i < purchasedItemList.size(); i++) {
					itemsId = itemsId + purchasedItemList.get(i);
					if (i < purchasedItemList.size() - 1)
						itemsId = itemsId + ",";
				}
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... params) {
				Login login = new Login(context);
				login.savePurchasedItem(userId, itemsId);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
			}
		}.execute();
	}

	/**
	 * Get math resut list
	 *
	 * @param context
	 * @return
	 */
	public static ArrayList<MathResultTransferObj> getResultMathList(Context context) {
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(context);
		learningCenterObj.openConn();
		ArrayList<MathResultTransferObj> mathResultList = learningCenterObj.getMathResultData();
		learningCenterObj.closeConn();
		return mathResultList;
	}

	/**
	 * Save temp player score on server which is recently added into user account at the time of
	 * registration or login
	 *
	 * @param context
	 * @param mathobj
	 * @param listener
	 * @param tempPlayerUserName
	 */
	public static void saveTempPlayerScoreOnServer(final Context context,
			final ArrayList<MathResultTransferObj> mathobj, 
			final OnRequestComplete listener
			, final String tempPlayerUserName) {

		new AsyncTask<Void, Void, Void>() {

			private String playerId = null;
			private String UserId = null;

			@Override
			protected void onPreExecute() {

				UserPlayerOperation userPlayerObj = new UserPlayerOperation(context);
				playerId = userPlayerObj.getPlayerIdbyUserName(tempPlayerUserName);
				userPlayerObj.closeConn();

				UserRegistrationOperation userOperation = new UserRegistrationOperation(context);
				UserId = userOperation.getUserData().getUserId();
				userOperation.closeConn();

				for (int i = 0; i < mathobj.size(); i++) {
					mathobj.get(i).setPlayerId(playerId);
					mathobj.get(i).setUserId(UserId);
				}

				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... params) {
				Register register = new Register(context);
				register.savePlayerScoreOnServer(mathobj);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				listener.onComplete();
			}
		}.execute();
	}

	/**
	 * Get the player equation data
	 *
	 * @param context
	 * @param playerId
	 * @return
	 */
	public static ArrayList<PlayerEquationLevelObj>
	getPlayerEquationData(Context context, String playerId) {
		LearningCenterimpl learnignCenter1 = new LearningCenterimpl(context);
		learnignCenter1.openConn();
		ArrayList<PlayerEquationLevelObj> playerquationData = learnignCenter1
				.getPlayerEquationLevelDataByPlayerId(playerId);
		learnignCenter1.closeConn();
		return playerquationData;
	}

	/**
	 * Add player level on server
	 *
	 * @param playerquationData
	 * @param listener
	 */
	public static void addLevelAsynckTask(final ArrayList<PlayerEquationLevelObj> playerquationData
			, final OnRequestComplete listener) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... params) {
				LearningCenteServerOperation serverOpr = new LearningCenteServerOperation();
				serverOpr.addAllLevel("<levels>" + getLevelXml(playerquationData) + "</levels>");
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				listener.onComplete();
			}
		}.execute();
	}

	/**
	 * Get the player xml at the time of registration
	 *
	 * @param playerquationData
	 * @return
	 */
	private static String getLevelXml(ArrayList<PlayerEquationLevelObj> playerquationData) {
		StringBuilder xml = new StringBuilder("");
		for (int i = 0; i < playerquationData.size(); i++) {
			xml.append("<userLevel>" +
					"<uid>" + playerquationData.get(i).getUserId() + "</uid>" +
					"<pid>" + playerquationData.get(i).getPlayerId() + "</pid>" +
					"<eqnId>" + playerquationData.get(i).getEquationType() + "</eqnId>" +
					"<level>" + playerquationData.get(i).getLevel() + "</level>" +
					"<stars>" + playerquationData.get(i).getStars() + "</stars>" +
					"</userLevel>");
		}
		return xml.toString();
	}


	/**
	 * Delete the result data from MathResult after it uploaded on the server
	 *
	 * @param context
	 */
	public static void deleteFromMath(Context context) {
		LearningCenterimpl learnignCenter = new LearningCenterimpl(context);
		learnignCenter.openConn();
		learnignCenter.deleteFromMathResult();
		learnignCenter.closeConn();
	}



	/**
	 * Get the player id , which is recenlty added into the user account at the time
	 * of registration
	 *
	 * @param contex
	 * @param tempPlayer
	 * @return
	 */
	public static String getPlayerIdOfTempPlayerWhichIsNowRegister
	(Context contex, TempPlayer tempPlayer) {
		UserPlayerOperation userOpr = new UserPlayerOperation(contex);
		String playerId = userOpr.getPlayerIdbyUserName(tempPlayer.getUserName());
		userOpr.closeConn();
		return playerId;
	}

	/**
	 * Create temp player
	 *
	 * @param userName
	 * @param context
	 */
	public static TempPlayer createDefaultPlayer(Context context, String fName, String lName,
			String city, int grade, SchoolDTO school, TeacherDTO teacher, String userName
			, String zipCode, String state, String country, String profileImageName) {

		TempPlayer tempPlayer = new TempPlayer();
		tempPlayer.setFirstName(fName);
		tempPlayer.setLastName(lName);
		tempPlayer.setCity(city);
		tempPlayer.setCoins(0);
		tempPlayer.setCompeteLevel(1);
		tempPlayer.setGrade(grade);
		tempPlayer.setParentUserId(0);//
		tempPlayer.setPlayerId(0);//
		tempPlayer.setPoints(0);

		tempPlayer.setProfileImageName(profileImageName);
		tempPlayer.setSchoolId(Integer.parseInt(school.getSchoolId()));
		tempPlayer.setSchoolIdString(school.getSchoolId());
		tempPlayer.setSchoolName(school.getSchoolName());

		tempPlayer.setTeacherFirstName(teacher.getfName());
		tempPlayer.setTeacherLastName(teacher.getlName());
		tempPlayer.setTeacherUserId(Integer.parseInt(teacher.getTeacherUserId()));
		tempPlayer.setUserName(userName);
		tempPlayer.setZipCode(zipCode);
		tempPlayer.setState(state);
		tempPlayer.setCountry(country);
		return tempPlayer;
	}

	/** Return the default schook with Can't find my school
	 *
	 * @return
	 */
	public static SchoolDTO getDefaultSchool() {
		SchoolDTO school = new SchoolDTO();
		school.setSchoolId("0000");
		school.setSchoolName("Cannot find my school");
		return school;
	}

	/**
	 * Return the default teacher with can not find my teacher
	 *
	 * @return
	 */
	public static TeacherDTO getDefaultTeacher() {
		TeacherDTO teacher = new TeacherDTO();
		teacher.setfName("Cannot find my teacher");
		teacher.setlName("");
		teacher.setTeacherUserId("-1");
		return teacher;
	}
	/**
	 * Check for temp player is created or not atleast one time in the app
	 *
	 * @param context
	 * @return
	 */
	public static boolean isTempraroryPlayerCreated(Context context) {
		TempPlayerOperation tempObj = new TempPlayerOperation(context);
		boolean isCreated = tempObj.isTemparyPlayerExist();
		tempObj.closeConn();
		return isCreated;
	}

	/**
	 * Create a blank temp player table
	 *
	 * @param contex
	 */
	public static void createBlankTempTable(Context contex) {
		try {
			TempPlayerOperation tempObj = new TempPlayerOperation(contex);
			tempObj.createTempPlayerTable();
			tempObj.closeConn();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get country ISO by country name
	 *
	 * @param countryName
	 * @param context
	 * @return
	 */
	public static String getCountryIsoByCountryName(String countryName, Context context) {
		Country countryObj = new Country();
		return countryObj.getCountryIsoByCountryName(countryName, context);
	}


	/**
	 * This method check for empty fields
	 *
	 * @param fields
	 * @return true , if any field is empty otherwise return false;
	 */
	public static boolean checkForEmptyFields(String... fields) {
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].length() == 0)
				return true;
		}
		return false;
	}


	/**
	 * Get Country id by country name
	 *
	 * @param countryName
	 * @param context
	 * @return
	 */
	public static String getCountryIdByCountryName(String countryName, Context context) {
		Country countryObj = new Country();
		return countryObj.getCountryIdByCountryName(countryName, context);
	}

	/**
	 * Return the selected grade
	 *
	 * @param selectedGrade
	 * @return
	 */
	public static int getGrade(String selectedGrade) {
		try {
			if (selectedGrade.equalsIgnoreCase(GRADE_TEXT)) {
				return NO_GRADE_SELETED;
			} else if (selectedGrade.equalsIgnoreCase(ADULT)) {
				return ADULT_GRADE;
			} else {
				return Integer.parseInt(selectedGrade);
			}
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Get grade for selection at the time of edit grade
	 *
	 * @param grade
	 * @return
	 */
	public static String getGradeForSelection(String grade) {
		try {
			int intGrade = Integer.parseInt(grade);
			if (intGrade == ADULT_GRADE)
				return ADULT;
			return grade + "";
		} catch (Exception e) {
			return 1 + "";
		}
	}

	/**
	 * Generate warning dialog
	 *
	 * @param context
	 * @param msg
	 */
	public static void warningDialog(Context context, String msg) {

		DialogGenerator dg = new DialogGenerator(context);
		dg.generateWarningDialog(msg);

	}

	/**
	 * Select Avatar
	 *
	 * @param context
	 * @param requestCode
	 */
	public static void selectAvatar(Context context, int requestCode) {
		Intent chooseAvtar = new Intent(context, ChooseAvtars.class);
		chooseAvtar.putExtra("isForNewRegistration", true);
		((Activity) context).startActivityForResult(chooseAvtar, requestCode);
	}

	/**
	 * Select teacher
	 *
	 * @param context
	 * @param schoolId
	 */
	public static void selectTeacher(Context context, String schoolId) {
		Intent teacherIntent = new Intent(context, SearchTeacherActivity.class);
		teacherIntent.putExtra("schoolId", schoolId);
		((Activity) context).startActivityForResult(teacherIntent, SELECT_TEACHER_REQUEST_CODE);
	}

	/**
	 * Select school
	 *
	 * @param selectedCountry
	 * @param city
	 * @param zipCode
	 * @param state
	 */
	public static void selectSchool(Context context,
			String selectedCountry, String city,
			String zipCode, String state) {

		Intent schoolIntent = new Intent(context, SearchYourSchoolActivity.class);
		if (selectedCountry.equals(UNITED_STATE) ||
				selectedCountry.toString().equals(CANADA)) {
			if (/*city.toString().equals("") ||*/
					zipCode.toString().equals("")) {
				DialogGenerator dg1 = new DialogGenerator(context);
				Translation transeletion1 = new Translation(context);
				transeletion1.openConnection();
				dg1.generateWarningDialog(transeletion1
						.getTranselationTextByTextIdentifier
						("alertMsgEnterYourLocationToContinue"));
				transeletion1.closeConnection();
			} else {
				schoolIntent.putExtra("country", selectedCountry);
				schoolIntent.putExtra("state", state);
				schoolIntent.putExtra("city", city);
				schoolIntent.putExtra("zip", zipCode);
				((Activity) context).startActivityForResult(schoolIntent,
						SELECT_SCHOOL_REQUEST_CODE);
			}
		} else {
			schoolIntent.putExtra("country", selectedCountry);
			schoolIntent.putExtra("state", "");
			schoolIntent.putExtra("city", city);
			schoolIntent.putExtra("zip", zipCode);
			((Activity) context).startActivityForResult
			(schoolIntent, SELECT_SCHOOL_REQUEST_CODE);
		}
	}

	/**
	 * Get the temp player data
	 *
	 * @param context
	 * @return
	 */
	public static TempPlayer getTempPlayer(Context context) {
		try {
			TempPlayerOperation tempPlayerOperationObj = new TempPlayerOperation(context);
			if (tempPlayerOperationObj.isTemparyPlayerExist()) {
				if (!tempPlayerOperationObj.isTempPlayerDeleted()) {
					TempPlayerOperation tempPlayerOperationObj1 = new TempPlayerOperation(context);
					ArrayList<TempPlayer> tempPlayer = tempPlayerOperationObj1.getTempPlayerData();
					return tempPlayer.get(0);

				} else {
					return null;
				}
			} else {
				tempPlayerOperationObj.closeConn();
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get the country list
	 *
	 * @param context
	 * @return
	 */
	public static ArrayList<String> getCountryList(Context context) {
		Country countryObj = new Country();
		ArrayList<String> countryList = countryObj.getCountryName(context);
		return countryList;
	}

	public static ArrayList<String> getUpdatedGradeList(Context context) {
		Grade gradeObj = new Grade();
		ArrayList<String> gradeList = gradeObj.getGradeList(context);
		gradeList.add(0, MathFriendzyHelper.getGradeText(context));
		gradeList.set(gradeList.size() - 1, MathFriendzyHelper.ADULT);
		return gradeList;
	}

	/**
	 * Grade text which add at the top of the grade list in the new registraion
	 *
	 * @param contex
	 * @return
	 */
	public static String getGradeText(Context contex) {
		return MathFriendzyHelper.GRADE_TEXT;
	}

	/**
	 * Set in focus change listener
	 *
	 * @param context
	 * @param edtText
	 */
	public static void setFocusChangeListener(final Context context, final EditText edtText,
			final EditTextFocusChangeListener listener) {
		edtText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					if (!isValidString(edtText.getText().toString())) {
						edtText.setText("");
						listener.onFocusLost(false);
						MathFriendzyHelper.showWarningDialog(context,
								getSpecialCharacterNotAllowedMessage(context));
					} else {
						listener.onFocusLost(true);
					}
				}
			}
		});
	}

	/**
	 * This method show the warning dialog
	 *
	 * @param context
	 * @param message
	 */
	public static void showWarningDialog(Context context, String message) {
		DialogGenerator dg = new DialogGenerator(context);
		dg.generateWarningDialog(message);
	}


	private static final String REGISTRATION_STRING_PATTERN = "(?=.*[\\\\<>\"&'])";
	protected static final String TAG = "MathFriendzyHelper";

	/**
	 * Check for valid string
	 *
	 * @param string , return true is a string does not contain any of the given special character ":\\<>"&'"
	 * @return otherwise false
	 */
	public static boolean isValidString(String string) {
		Pattern p = Pattern.compile(REGISTRATION_STRING_PATTERN,
				Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(string);
		return !m.find();
	}


	/**
	 * Retutn the special character message
	 *
	 * @param context
	 * @return
	 */
	private static String getSpecialCharacterNotAllowedMessage(Context context) {
		String lblSomeSpecialCharAreNotAllowed = null;
		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		lblSomeSpecialCharAreNotAllowed = transeletion
				.getTranselationTextByTextIdentifier("lblSomeSpecialCharAreNotAllowed");
		transeletion.closeConnection();
		return lblSomeSpecialCharAreNotAllowed;
	}

	/**
	 * Set country adapter
	 *
	 * @param context
	 * @param selectedCountry
	 * @param countryList
	 * @param spinner
	 */
	public static void setCountryAdapter(Context context, String selectedCountry,
			ArrayList<String> countryList, Spinner spinner) {
		if (countryList != null) {
			ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>
			(context, R.layout.spinner_country_item, countryList);
			countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(countryAdapter);
			spinner.setSelection(countryAdapter.getPosition(selectedCountry));
		}
	}

	/**
	 * Set country adapter
	 *
	 * @param context
	 * @param spinner
	 */
	public static void setAdapterToSpinner(Context context, String selectionValue,
			ArrayList<String> list, Spinner spinner) {
		if (list != null) {
			ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>
			(context, R.layout.spinner_country_item, list);
			countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(countryAdapter);
			spinner.setSelection(countryAdapter.getPosition(selectionValue));
		}
	}

	/**
	 * Set Checked , Set check player if only one player exist in his account
	 *
	 * @param contxt
	 */
	public static void setCheckPlayer(Context contxt) {
		SharedPreferences sheredPreference1 = contxt.getSharedPreferences(ICommonUtils.IS_CHECKED_PREFF, 0);
		SharedPreferences.Editor editor = sheredPreference1.edit();
		editor.clear();

		UserPlayerOperation userObj = new UserPlayerOperation(contxt);
		ArrayList<UserPlayerDto> userPlayerObj = userObj.getUserPlayerData();
		if (userPlayerObj.size() == 1) {
			editor.putString(ICommonUtils.PLAYER_ID, userPlayerObj.get(0).getPlayerid());
			editor.putString("userId", userPlayerObj.get(0).getParentUserId());

		}
		editor.commit();
	}


	/**
	 * Open the Main Screen after registration success
	 *
	 * @param contex
	 * @param email
	 */
	public static void openMainScreenAfterRegistration(Context contex, String email) {
		try {
			//Need to do for disable ads after registration
			MathFriendzyHelper.getPurchasedItemDetail(contex,
					MathFriendzyHelper.getUserId(contex));
		}catch(Exception e){
			e.printStackTrace();
		}

		Intent intent = new Intent(contex, MainActivity.class);
		if (email.equals(""))//do not show registration pop up for student
			intent.putExtra("isCallFromRegistration", false);
		else
			intent.putExtra("isCallFromRegistration", true);
		contex.startActivity(intent);
	}


	/**
	 * Clear focus
	 *
	 * @param context
	 */
	public static void clearFocus(Context context) {
		try {
			((Activity) context).getCurrentFocus().clearFocus();
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Get purchased item detail
	 *
	 * @param context
	 * @param userId
	 */
	public static void getPurchasedItemDetail(final Context context, final String userId) {
		new AsyncTask<Void, Void, Void>() {
			private String resultString = "";

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... params) {
				MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
				resultString = serverObj.getPurchasedItemDetailByUserId(userId);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				try {
					if (resultString != null) {
						String itemIds = "";
						ArrayList<String> itemList = new ArrayList<String>();

						if (resultString != null) {//start if
							//got app unlock status , changes on 5 May 2014
							String[] resultValue = resultString.split("&");
							resultString = resultValue[0].replace("&", "");
							String appUnlockStatus = resultValue[1].replace("&", "");

							if (resultString.length() > 0) {
								//itemIds = result.getItems().replace(",", "");
								//get item value which is comma separated , add (,) at the last for finish	
								String newItemList = resultString + ",";

								for (int i = 0; i < newItemList.length(); i++) {
									if (newItemList.charAt(i) == ',') {
										itemList.add(itemIds);
										itemIds = "";
									} else {
										itemIds = itemIds + newItemList.charAt(i);
									}
								}
							}

							ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();


							for (int i = 0; i < itemList.size(); i++) {
								PurchaseItemObj purchseObj = new PurchaseItemObj();
								purchseObj.setUserId(userId);
								purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
								purchseObj.setStatus(1);
								purchaseItem.add(purchseObj);
							}

							//add app unlick statuc , changes on 5 May 2014
							try {
								if (!appUnlockStatus.equals("-1")) {
									PurchaseItemObj purchseObj = new PurchaseItemObj();
									purchseObj.setUserId(userId);
									purchseObj.setItemId(100);
									purchseObj.setStatus(Integer.parseInt(appUnlockStatus));
									purchaseItem.add(purchseObj);
								}
							} catch (Exception e) {
								Log.e(TAG, "Error while inserting app unlock status " + e.toString());
							}

							LearningCenterimpl learningCenter = new LearningCenterimpl(context);
							learningCenter.openConn();
							learningCenter.deleteFromPurchaseItem();
							learningCenter.insertIntoPurchaseItem(purchaseItem);
							learningCenter.closeConn();
						}//end if
					}
				} catch (Exception e) {
					Log.e(TAG, "Error while getting purchase response " + e.toString());
				}
				super.onPostExecute(result);
			}
		}.execute();
	}


	/**
	 * Return avtar image bitmap by name
	 *
	 * @param context
	 * @param imageName
	 * @return
	 */
	public static Bitmap getAvatarImageByName(Context context, String imageName) {
		try {
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(context);
			Bitmap profileImageBitmap = null;
			if (chooseAvtarObj.getAvtarImageByName(imageName) != null) {
				profileImageBitmap = MathFriendzyHelper.getBitmapFromByte
						(chooseAvtarObj.getAvtarImageByName(imageName));
			}
			chooseAvtarObj.closeConn();
			return profileImageBitmap;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * This metho return the userId
	 * @param context
	 * @return
	 */
	public static String getUserId(Context context){
		//for login player
		SharedPreferences sheredPreference = context.getSharedPreferences(ICommonUtils.LOGIN_SHARED_PREFF, 0);
		if(sheredPreference.getBoolean(ICommonUtils.IS_LOGIN, false)){
			UserRegistrationOperation userObj = new UserRegistrationOperation(context);
			String userId = userObj.getUserId();
			return userId;
		}else{//for temp player
			return "";
		}
	}

	/**
	 * This method return bitmap from byte array
	 *
	 * @param byteImage
	 * @return
	 */
	public static Bitmap getBitmapFromByte(byte[] byteImage) {
		final int MIN_BYTE = 0;// for min byte
		Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, MIN_BYTE,
				byteImage.length);
		return bmp;
	}

	/**
	 * Initialize the progress dialog which will show
	 * at the time of login
	 *
	 * @param context
	 */
	public static void initializeProcessDialog(Context context) {
		//if(pd == null){
		pd = new ProgressDialog(context);
		pd.setIndeterminate(false);//back button does not work
		pd.setCancelable(false);
		pd.setMessage("Please Wait...");
		//}
	}

	/**
	 * Convert the string into underline formate
	 *
	 * @param string
	 * @return
	 */
	public static String convertStringToUnderLineString(String string) {
		return "<html>" + "<u>" + string + "</u></html>";
	}


	/**
	 * This method set the done button in keyboard for edit text
	 * @param edtText
	 */
	public static void setDoneButtonToEditText(EditText edtText){
		try {
			edtText.setImeOptions(EditorInfo.IME_ACTION_DONE);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Check for empty string+
	 *
	 * @param string
	 * @return
	 */
	public static boolean isEmpty(String string) {
		if (string != null && string.length() > 0)
			return false;
		return true;
	}

	/**
	 * Show the progress dialog
	 */
	public static void showDialog() {
		if (pd != null)
			pd.show();
	}
	/**
	 * Dismiss progress dialog
	 */
	public static void dismissDialog() {
		if (pd != null && pd.isShowing())
			pd.cancel();
	}

	/**
	 * Check for valid user name
	 *
	 * @param userName
	 * @param listener
	 */
	public static void checkForValidUserName(final String userName,
			final OnRequestCompleteWithStringResult listener) {

		new AsyncTask<Void, Void, Void>() {
			private String resultValue = null;

			@Override
			protected void onPreExecute() {
				//progressDialog.show();
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... params) {
				Validation validObj = new Validation();
				resultValue = validObj.validUser(userName);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				listener.onComplete(resultValue);
				super.onPostExecute(result);
			}
		}.execute();
	}

	/**
	 * Show dialog when user is already exist
	 *
	 * @param context
	 */
	public static void showAlreadyExistUserNameDialog(Context context) {
		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(context);
		dg.generateWarningDialog(transeletion.
				getTranselationTextByTextIdentifier("alertMsgThisUserNameAlreadyExist"));
		transeletion.closeConnection();
	}

	/**
	 * This method return the selected player id , if exist otherwise return blank id
	 *
	 * @param context
	 * @return
	 */
	public static String getSelectedPlayerID(Context context) {
		return context.getSharedPreferences(ICommonUtils.IS_CHECKED_PREFF, 0)
				.getString(ICommonUtils.PLAYER_ID, "");
	}

	/**
	 * This method return the Seletced player data
	 *
	 * @param context
	 * @param playerId
	 * @return
	 */
	public static UserPlayerDto getSelectedPlayerDataById(Context context, String playerId) {
		UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
		UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(playerId);
		return userPlayerData;
	}

	/**
	 * Show login registration dialog
	 *
	 * @param context
	 */
	public static void showLoginRegistrationDialog(Context context, String msg) {
		DialogGenerator dg = new DialogGenerator(context);
		dg.generateRegisterOrLogInDialog(msg);
	}

	/**
	 * Show login registration dialog
	 *
	 * @param context
	 */
	public static void showLoginRegistrationDialog(Context context) {
		DialogGenerator dg = new DialogGenerator(context);
		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		String alertMsgYouMustRegisterOrLogin = transeletion
				.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin");
		dg.generateRegisterOrLogInDialog(alertMsgYouMustRegisterOrLogin);
		transeletion.closeConnection();
	}

	/**
	 * Select Avatar
	 *
	 * @param context
	 * @param requestCode
	 */
	public static void selectAvatar(Context context, int requestCode, boolean isEditAvatar) {
		Intent chooseAvtar = new Intent(context, ChooseAvtars.class);
		chooseAvtar.putExtra("isForNewRegistrationSelectAvatar", true);
		((Activity) context).startActivityForResult(chooseAvtar, requestCode);
	}

	/**
	 * Parse the string into int
	 *
	 * @param stringValue
	 * @return
	 */
	public static int parseInt(String stringValue) {
		try {
			return Integer.parseInt(stringValue);
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * This method check for user is login or not
	 *
	 * @param context
	 * @return
	 */
	public static boolean isUserLogin(Context context) {
		return context.getSharedPreferences(ICommonUtils.LOGIN_SHARED_PREFF, 0)
				.getBoolean(ICommonUtils.IS_LOGIN, false);
	}

	/**
	 * Save the purchase subscription date
	 *
	 * @param context
	 * @param date
	 */
	public static void savePurchaseSubscriptionDate(Context context, String date) {
		if (CommonUtils.LOG_ON)
			Log.e(TAG, "subscriptionDate Date " + date);
		SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putString("purchaseSubscriptionDate", date);
		editor.commit();
	}

	/**
	 * Return the purchase subscription date
	 *
	 * @param context
	 * @return
	 */
	public static String getPurchaseSubscriptionDate(Context context) {
		return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
				.getString("purchaseSubscriptionDate", null);
	}

	public static void saveIsAdDisble(Context context , int value){
		SharedPreferences sharedPreff = context.getSharedPreferences
				(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putInt(ICommonUtils.ADS_IS_ADS_DISABLE,value);
		editor.commit();
	}

	/**
	 * Update teacher player avtar and grade
	 *
	 * @param context
	 * @param avatarName
	 * @param grade
	 * @param playerId
	 */
	public static void updateTeacherplayer(Context context,
			String avatarName, String grade, String playerId, String firstName, String lastName) {
		if (CommonUtils.LOG_ON)
			Log.e(TAG, "inside updateTeacherplayer player id " + playerId);
		UserPlayerOperation userPlayoprObj = new UserPlayerOperation(context);
		userPlayoprObj.updatePlayerGradeAvatar(playerId, grade, avatarName, firstName, lastName);
		userPlayoprObj.closeConn();
	}


	/**
	 * Get laguage translation
	 * @param context
	 * @param langId
	 * @param langCode
	 * @param appId
	 * @param responseIntefrface
	 */
	public static void getLanguageTranslation(final Context context ,
			final String langId , final String langCode ,
			final String appId ,
			final HttpServerRequest responseIntefrface){

		if(langId.equals(getDownloadedLangId(context))){
			responseIntefrface.onRequestComplete();
			return ;
		}else{
			//changes for New Assessment
			//            AssessmentTestImpl implObj = new AssessmentTestImpl(context);
			//            implObj.openConnection();
			//            implObj.deleteFromWordAssessmentStandards();
			//            implObj.deleteFromWordAssessmentSubStandards();
			//            implObj.deleteFromWordAssessmentSubCategoriesInfo();
			//            implObj.closeConnection();
			//end changes
			CommonUtils.LANGUAGE_CODE = langCode;
			CommonUtils.LANGUAGE_ID   = langId;
			saveDownloadedLanguage(context , langId);
		}

		new AsyncTask<Void,Void,Void>(){
			private ProgressDialog progressDialog = null;
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				progressDialog = CommonUtils.getProgressDialog(context);
				progressDialog.show();
			}

			@Override
			protected Void doInBackground(Void... params) {
				Translation traneselation = new Translation(context);
				traneselation.getTransalateTextFromServer(langCode, langId, appId);
				traneselation.saveTranslationText();
				return null;
			}

			@Override
			protected void onPostExecute(Void aVoid) {
				if(progressDialog != null
						&& progressDialog.isShowing())
					progressDialog.cancel();
				responseIntefrface.onRequestComplete();
				super.onPostExecute(aVoid);
			}
		}.execute();
	}

	/**
	 * get the downloaded language at the time of downloading
	 * @param context
	 * @return
	 */
	public static String getDownloadedLangId(Context context){
		return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
				.getString("langId", "");
	}

	/**
	 * Save the downloaded language at the time of downloading
	 * @param context
	 * @param langId
	 */
	public static void saveDownloadedLanguage(Context context , String langId){
		SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putString("langId", langId);
		editor.commit();
	}

	/**
	 * Get email valid value
	 * @param context
	 * @return
	 */
	public static String getEmailValid(Context context){
		return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
				.getString("emailValid", "");
	}

	/**
	 * Get email subscription
	 * @param context
	 * @return
	 */
	public static String getEmailSubscription(Context context){
		return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
				.getString("emailSubscription", "");
	}

	public static void showToast(Context context , String message){
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

	public static void saveBooleanValueInSharedPreff(Context context , 
			String key , boolean value){
		SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static boolean getBooleanValueFromPreff(Context context , String key){
		return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
				.getBoolean(key, false);
	}

	public static void saveIntegerValue(Context context , 
			String key , int value){
		SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static int getIntValueFromPreff(Context context , String key){
		return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0).getInt(key, 0);
	}

	public static void saveStringValueToPreff(Context context , 
			String key , String value){
		SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String getStringValueFromPreff(Context context , String key){
		return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0).getString(key, null);
	}

	public static void openUrlWithoutHttps(Context context , String url){
		if(!url.contains("http")){
			url = "http://" + url;
		}
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		context.startActivity(intent);    	
	}   

	/**
	 * Check for given key in the json object
	 * @param jsonObj
	 * @param key
	 * @return
	 */
	public static boolean checkForKeyExistInJsonObj(JSONObject jsonObj , String key){
		try{
			return jsonObj.has(key);
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Save the subscription expire date
	 *
	 * @param context
	 * @param date
	 */
	public static void saveSubscriptionExpireDate(Context context, String date) {   
		SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putString("subscriptionExpireDate", date);
		editor.commit();
	}

	/**
	 * Save email subscription and email valid for the weekly report on the account page
	 * @param context
	 * @param emailSubscription
	 * @param emailValid
	 */
	public static void saveEmailSubscriptionAndValid(Context context ,
			String emailSubscription ,
			String emailValid){
		SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putString("emailSubscription", emailSubscription);
		editor.putString("emailValid", emailValid);
		editor.commit();
	}

	/**
	 * Return the subscription expire date
	 *
	 * @param context
	 * @return
	 */
	public static String getSubscriptionExpireDate(Context context) {
		return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
				.getString("subscriptionExpireDate", null);
	}

	/**
	 * Validate the date in given formate
	 *
	 * @param date
	 * @param format
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static Date getValidateDate(String date, String format) {
		try {
			if(date != null)
				return new SimpleDateFormat(format).parse(date);
			return null;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressLint({"InlinedApi", "NewApi"})
	public static int getFreeSubscriptionRemainingDays(Context context) {
		try {
			String expireDateString = getSubscriptionExpireDate(context);
			Date currentDate = getCurrentDate(YYYY_MM_DD);
			Date expireDate = getValidateDateForTimeZone(expireDateString, YYYY_MM_DD);
			return (int) getDateDiff(currentDate, expireDate, TimeUnit.DAYS);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public static int getFreeSubscriptionRemainingDays(Context context, String dateString
			, String format) {
		try {
			Date currentDate = getCurrentDate(YYYY_MM_DD);
			Date expireDate = getValidateDateForTimeZone(dateString, format);
			return (int) getDateDiff(currentDate, expireDate, TimeUnit.DAYS);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Validate the date in given formate
	 *
	 * @param date
	 * @param format
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	private static Date getValidateDateForTimeZone(String date, String format) {
		try {
			SimpleDateFormat formate = new SimpleDateFormat(format);
			formate.setTimeZone(TimeZone.getTimeZone("PDT"));
			return formate.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get a diff between two dates
	 *
	 * @param date1    the oldest date
	 * @param date2    the newest date
	 * @param timeUnit the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		//diffInMillies -= getTimeOffset(diffInMillies);
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
		//return diffInMillies / (24 * 60 * 60 * 1000);
	}

	/**
	 * Return the currect date in given format
	 *
	 * @param formate
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	private static Date getCurrentDate(String formate) {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat(formate);
		try {
			df.setTimeZone(TimeZone.getTimeZone("PDT"));
			return df.parse(df.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get us stateid by name
	 *
	 * @param stateName
	 * @param context
	 * @return
	 */
	public static String getUsStateIdByName(String stateName, Context context) {
		States stateobj = new States();
		String stateId = stateobj.getUSStateIdByName(stateName, context);
		return stateId;
	}

	/**
	 * Get US state code by state name
	 *
	 * @param stateName
	 * @param context
	 * @return
	 */
	public static String getUsStateCodeByName(String stateName, Context context) {
		States stateobj = new States();
		String stateCode = stateobj.getStateCodeNameByStateNameFromUS(stateName, context);
		return stateCode;
	}

	/**
	 * Get canada state id by state name
	 *
	 * @param stateName
	 * @param context
	 * @return
	 */
	public static String getCanadaStateIdByName(String stateName, Context context) {
		States stateobj = new States();
		String stateId = stateobj.getCanadaStateIdByName(stateName, context);
		return stateId;
	}

	/**
	 * Get canada state code by state name
	 *
	 * @param stateName
	 * @param contex
	 * @return
	 */
	public static String getCanadaStateCodeByName(String stateName, Context contex) {
		States stateobj = new States();
		String stateCode = stateobj.getStateCodeNameByStateNameFromCanada(stateName, contex);
		return stateCode;
	}

	/**
	 * Return the currect login user account type
	 *
	 * @param context
	 * @return
	 */
	public static int getUserAccountType(Context context) {
		try {
			UserRegistrationOperation userObj = new UserRegistrationOperation(context);
			RegistereUserDto regUserObj = userObj.getUserData();
			if (regUserObj.getIsParent().equals("2")) {//2 for student
				return STUDENT;
			} else if (regUserObj.getIsParent().equals("1")) {// 1 for parent
				return PARENT;
			} else if (regUserObj.getIsParent().equals("0")) {//0 for teacher
				return TEACHER;
			} else {
				return TEACHER;
			}
		} catch (Exception e) {
			return PARENT;
		}
	}

	/**
	 * @param date           - date
	 * @param currentFormate - current date formate
	 * @param requiredFormat - new required date formate
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String formatDataInGivenFormat(String date,
			String currentFormate, String requiredFormat) {
		try {
			SimpleDateFormat givenFormat = new SimpleDateFormat(currentFormate);
			SimpleDateFormat convertedFomate = new SimpleDateFormat(requiredFormat);
			return convertedFomate.format(givenFormat.parse(date));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void sendFeedback(Context context , 
			final String fromEmail , final String feebback , final String appName){	    	 
		new AsyncTask<Void, Void, Void>(){

			@Override
			protected void onPreExecute() {

			}

			@Override
			protected Void doInBackground(Void... params) {
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				try{
					nameValuePairs.add(new BasicNameValuePair("action", "sendAppFeedback"));
					nameValuePairs.add(new BasicNameValuePair("from", fromEmail));
					nameValuePairs.add(new BasicNameValuePair("message", feebback));
					nameValuePairs.add(new BasicNameValuePair("appName", appName));
					CommonUtils.readFromURL(nameValuePairs);
				}catch(Exception e){
					e.printStackTrace();
				}				
				return null;				
			}

			@Override
			protected void onPostExecute(Void result) {

			}
		}.execute();
	}

	public static int getNonZeroValue(int value){
		if(value == 0)
			return 1;
		return value;
	}

	public static void initializeIsSuscriptionPurchase(boolean bValue){
		isSubscriptionPurchase = bValue;
	}

	public static boolean isSubscriptionPurchase(){
		return isSubscriptionPurchase;
	}

	/**
	 * Return the device id
	 *
	 * @param context
	 * @return
	 */
	@SuppressWarnings("unused")
	public static String getDeviceId(Context context) {
		try {
			String myAndroidDeviceId = "";
			TelephonyManager mTelephony = (TelephonyManager) context.
					getSystemService(Context.TELEPHONY_SERVICE);
			if (mTelephony.getDeviceId() != null) {
				myAndroidDeviceId = mTelephony.getDeviceId();
			} else {
				myAndroidDeviceId = Settings.Secure.
						getString(context.getApplicationContext().getContentResolver(),
								Settings.Secure.ANDROID_ID);
			}
			if(AppVersion.CURRENT_VERSION == AppVersion.PAID_VERSION){
				myAndroidDeviceId = myAndroidDeviceId + "_paid";
			}
			return myAndroidDeviceId;
		} catch (Exception e) {
			return "";
		}
	}

	public static String getStudentName(String firstName , String lastName){
		try{
			if(!MathFriendzyHelper.isEmpty(lastName)){
				return firstName + " " + lastName.charAt(0);
			}
			return firstName;
		}catch(Exception e){
			e.printStackTrace();
			return firstName + " " + lastName;
		}
	}

	/**
	 * Save user Login
	 *
	 * @param context
	 * @param bValues
	 */
	public static void saveUserLogin(Context context, boolean bValues) {
		SharedPreferences sheredPreference =
				context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		SharedPreferences.Editor editor = sheredPreference.edit();
		editor.putBoolean(IS_LOGIN, bValues);
		editor.commit();
	}

	//change for not to create temp player
	public static void openCreateTempPlayerActivity(Context context){
		/*Intent intent = new Intent(context,CreateTempPlayerActivity.class);
    	context.startActivity(intent);*/    	
		if(!MathFriendzyHelper.isUserLogin(context)){
			MathFriendzyHelper.showLoginRegistrationDialog(context);			
		}    	
	}

	public static String getAppName(Translation transeletion) {
		String appName = ITextIds.LEVEL+" "+transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE);
		return appName;
	}

	public static void downloadMoreApps(final Context context , 
			final MoreAppListener listener , final String appId){
		new AsyncTask<Void,Void,ArrayList<AppDetail>>(){
			private ProgressDialog progressDialog = null;
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				progressDialog = CommonUtils.getProgressDialog(context);
				progressDialog.show();
			}

			@Override
			protected ArrayList<AppDetail> doInBackground(Void... params) {
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				try{
					nameValuePairs.add(new BasicNameValuePair("action", "getApps"));
					nameValuePairs.add(new BasicNameValuePair("udid", ""));
					nameValuePairs.add(new BasicNameValuePair("android", "1"));	
					return MoreAppFileParser.parseAppData(CommonUtils
							.readFromURL(nameValuePairs) , appId);
				}catch(Exception e){
					e.printStackTrace();
				}				
				return null;
			}

			@Override
			protected void onPostExecute(ArrayList<AppDetail> appDetailList) {
				if(progressDialog != null
						&& progressDialog.isShowing())
					progressDialog.cancel();
				listener.onSuccess(appDetailList);
				super.onPostExecute(appDetailList);
			}
		}.execute();
	}

	public static ImageLoader getImageLoaderInstance(){
		return ImageLoader.getInstance();
	}

	/**
	 * Get display option
	 * @return
	 */
	public static DisplayImageOptions getDisplayOption(){
		return new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.resources)
		.showImageForEmptyUri(R.drawable.resources)
		.showImageOnFail(R.drawable.resources)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.considerExifParams(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

	/** Open another app.
	 * @param context current Context, like Activity, App, or Service
	 * @param packageName the full package name of the app to open
	 * @return true if likely successful, false if unsuccessful
	 */
	public static boolean openApp(Context context, String packageName) {
		PackageManager manager = context.getPackageManager();
		try {
			Intent i = manager.getLaunchIntentForPackage(packageName);
			if (i == null) {
				return false;
			}
			i.addCategory(Intent.CATEGORY_LAUNCHER);
			context.startActivity(i);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Return the installed application package list
	 * @param context
	 * @return
	 */
	public static ArrayList<String> getInstalledAppInAndroid(Context context){
		final PackageManager pm = context.getPackageManager();
		ArrayList<String> pachageList = new ArrayList<String>();
		List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
		for (ApplicationInfo packageInfo : packages) {
			/*Log.d("TAG", "Installed package :" + packageInfo.packageName);
		    Log.d("TAG", "Source dir : " + packageInfo.sourceDir);
		    Log.d("TAG", "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName));*/
			pachageList.add(packageInfo.packageName);
		}
		return pachageList;		
	}

	/**
	 * Get display option
	 * @return
	 */
	public static DisplayImageOptions getDisplayOptionWithoutCaching(){
		return new DisplayImageOptions.Builder()
		.showImageOnLoading(null)
		.showImageForEmptyUri(null)
		.showImageOnFail(null)
		.considerExifParams(true)
		//.cacheInMemory(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}	

	/**
	 * Update the image with .png formate
	 *
	 * @param imageName
	 * @return
	 */
	public static String getPNGFormateImageName(String imageName) {
		if (imageName.contains(".png")) {
			return imageName;
		} else {
			return imageName + ".png";
		}
	}

	public static void downloadCustomeHouseAdsData(Context context , 
			final OnAdsDataFromServerSuccess listener){
		if(!CommonUtils.isInternetConnectionAvailable(context)){
			return ;
		}

		new AsyncTask<Void, Void, CustomeHouseAdsData>() {
			@Override
			protected void onPreExecute() {
			}

			@Override
			protected CustomeHouseAdsData doInBackground(Void... params) {	
				try{
					ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("action", "getApps"));
					nameValuePairs.add(new BasicNameValuePair("udid", ""));
					nameValuePairs.add(new BasicNameValuePair("android", "1"));	
					return AdsResponseFileParser.parseCustomeHouseAdsData
							(CommonUtils.readFromURL(nameValuePairs));
				}catch(Exception e){
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPostExecute(CustomeHouseAdsData result) {
				listener.onAdsData(result);
			}
		}.execute();
	}

	public static void removeHouseAdsImageFromCache(Context context){
		String url = ICommonUtils.HOUSE_ADS_IMAGE_URL + 
				MathFriendzyHelper.getPNGFormateImageName(getHouseAdsImageName(context));
		MemoryCacheUtils.removeFromCache(url, ImageLoader.getInstance()
				.getMemoryCache());		
	}

	public static String getHouseAdsImageName(Context context){
		String imageName = MathFriendzyHelper.getStringValueFromPreff(context, 
				MathFriendzyHelper.HOUSE_ADS_IMAGE_NAME);
		if(MathFriendzyHelper.isEmpty(imageName))
			return DEFAULT_HOUSE_ADS_IMAGE_NAME;
		return imageName;
	}

	public static String getHouseAdsUrlLink(Context context){
		String link = MathFriendzyHelper.getStringValueFromPreff(context, 
				MathFriendzyHelper.HOUSE_ADS_LINK_URL);
		if(MathFriendzyHelper.isEmpty(link))
			return DEFAULT_HOUSE_ADS_LINK_URL;
		return link;
	}

	/**
	 * This method return the user player data
	 * @return
	 */
	public static UserPlayerDto getPlayerData(Context context){
		try{
			return MathFriendzyHelper.getSelectedPlayerDataById
					(context, MathFriendzyHelper.getSelectedPlayerID(context));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public static boolean isStudentRegisterByTeacher(UserPlayerDto selectedPlayer){
		try{
			if(selectedPlayer != null 
					&& !MathFriendzyHelper
					.isEmpty(selectedPlayer.getStudentIdByTeacher())){
				if(MathFriendzyHelper
						.parseInt(selectedPlayer.getStudentIdByTeacher()) > 0){
					return true;
				}
			}
			return false;
		}catch(Exception e){
			return false;
		}
	}	

	public static String getAppNameForNotification(){
		/*if(AppVersion.CURRENT_VERSION == AppVersion.PAID_VERSION){				
			return "1st Grade Unlocked";
		}
		return  "1st Grade Friendzy";*/
		return MyApplication.getAppContext().getResources().getString(R.string.app_name);

	}

	public static String returnTextFromTextview(TextView tv){
		return tv.getText().toString();
	}

	//----------------------------------------------------------//
	//New Changes on Jan 2016
	public static final String SUCCESS = "success";
	public static final int APP_UNLOCK_ID = 100;//id for full app unlock
	public static final int APP_UNLOCK = 1;
	public static final int NO_RESPONSE_FROM_SERVER_REQUEST_CODE = -1;
	public static final int SHOW_UNLOCK_FOR_LEARNING_CENTER = 1;
	public static final int SHOW_UNLOCK_FOR_SINGLE_FRIENDZY = 2;
	public static final int SHOW_UNLOCK_FOR_MULTI_FRIENDZY = 3;
	public static final int SHOW_PURCHASE_RESOURCE_VIDEO = 4;
	public static final String RESOURCE_PURCHASE_STATUS_KEY = "com.mathfriendzy.resource.purchase.key";
	public static final String UNLOCK_CATEGORY_PURCHASE_STATUS_KEY = "com.mathfriendzy.unlock_category.purchase.key";
	public static final int MAX_USER_COINS_TO_UNLOCK_APP_CATEGORY = 10000;

	public static ProgressDialog getProgressDialog(Context context , String message){
		ProgressDialog pd = new ProgressDialog(context);
		pd.setIndeterminate(false);//back button does not work
		pd.setCancelable(false);
		pd.setMessage(message);
		return pd;
	}

	public static void showProgressDialog(ProgressDialog pd){
		if(pd != null && !pd.isShowing()){
			pd.show();
		}
	}

	public static void hideProgressDialog(ProgressDialog pd){
		if(pd != null && pd.isShowing()){
			pd.cancel();
		}
	}

	public static void getAppUnlockStatusFromServerAndSaveIntoLocal(final Context context
			, final String userId , boolean isShowDialog , final HttpResponseInterface responseInterface){
		if(CommonUtils.isInternetConnectionAvailable(context)) {
			GetAppUnlockStatusParam param = new GetAppUnlockStatusParam();
			param.setAction("getStatusOfSubscriptionForUser");
			param.setUserId(userId);
			new MyAsyckTask(ServerOperation.CreatePostRequestForGetAppUnlockStatus(param)
					, null, ServerOperationUtil.GET_APP_UNLOCK_STATUS_REQUEST, context,
					new HttpResponseInterface() {
				@Override
				public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
					GetAppUnlockStatusResponse response =
							(GetAppUnlockStatusResponse) httpResponseBase;
					if(response.getResult()
							.equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
							&& response.getAppUnlockStatus() != -1){
						ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
						PurchaseItemObj purchseObj = new PurchaseItemObj();
						purchseObj.setUserId(userId);
						purchseObj.setItemId(MathFriendzyHelper.APP_UNLOCK_ID);
						purchseObj.setStatus(response.getAppUnlockStatus());
						purchaseItem.add(purchseObj);
						LearningCenterimpl learningCenter = new LearningCenterimpl(context);
						learningCenter.openConn();
						learningCenter.deleteFromPurchaseItem(userId ,
								MathFriendzyHelper.APP_UNLOCK_ID);
						learningCenter.insertIntoPurchaseItem(purchaseItem);
						learningCenter.closeConn();
					}
					responseInterface.serverResponse(httpResponseBase , requestCode);
				}
			}, ServerOperationUtil.SIMPLE_DIALOG, isShowDialog,
			context.getString(R.string.please_wait_dialog_msg))
			.execute();
		}else{
			if(isShowDialog) {
				CommonUtils.showInternetDialog(context);
			}
			responseInterface.serverResponse(null ,
					MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
		}
	}

	public static void getInAppStatusAndShowDialog(final Context context ,final int requestFrom ,
			final HttpResponseInterface responseInterface ,
			boolean ishowDialog ,
			final GetAppUnlockStatusResponse appUnlockResponse ,
			final OnPurchaseDone onPurchaseDone){
		if(!CommonUtils.isInternetConnectionAvailable(context)){
			if(ishowDialog) {
				CommonUtils.showInternetDialog(context);
			}
			responseInterface.serverResponse(null ,
					MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
			return ;
		}

		if(MathFriendzyHelper.isUserLogin(context)){
			MathFriendzyHelper.getVideoInAppStatusSaveIntoSharedPreff(context ,
					MathFriendzyHelper.getUserId(context) , new HttpResponseInterface() {
				@Override
				public void serverResponse(final HttpResponseBase httpResponseBase,
						final int requestCode) {
					if(requestFrom != SHOW_PURCHASE_RESOURCE_VIDEO){
						if(MathFriendzyHelper
								.isAppUnlockCategoryPurchased(context)){
							responseInterface.serverResponse(httpResponseBase , requestCode);
						}else{
							responseInterface.serverResponse(null ,
									MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
							if(appUnlockResponse.getCoins() >= MAX_USER_COINS_TO_UNLOCK_APP_CATEGORY){
								MathFriendzyHelper.showUnlockCategoryDialogWhenUserHaveMaxCoins(
										context , onPurchaseDone , appUnlockResponse);
							}else {
								MathFriendzyHelper.showUnlockMathCategoryDialog(context, requestFrom);
							}
						}
					}else if(requestFrom == SHOW_PURCHASE_RESOURCE_VIDEO){
						if(MathFriendzyHelper
								.isAppUnlockOrResourcePurchased(context)){
							responseInterface.serverResponse(httpResponseBase , requestCode);
						}else{
							responseInterface.serverResponse(null ,
									MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
						}
					}
				}
			} , ishowDialog);
		}else{
			if(requestFrom != SHOW_PURCHASE_RESOURCE_VIDEO) {
				responseInterface.serverResponse(null,
						MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
				if(appUnlockResponse.getCoins() >= MAX_USER_COINS_TO_UNLOCK_APP_CATEGORY){
					MathFriendzyHelper.showUnlockCategoryDialogWhenUserHaveMaxCoins(
							context , onPurchaseDone , appUnlockResponse);
				}else {
					MathFriendzyHelper.showUnlockMathCategoryDialog(context, requestFrom);
				}
			}else if(requestFrom == SHOW_PURCHASE_RESOURCE_VIDEO){
				responseInterface.serverResponse(null ,
						MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
			}
		}
	}

	/**
	 * Get the Resource vedio In-App Status
	 * @param context
	 * @param userId
	 */
	public static void getVideoInAppStatusSaveIntoSharedPreff(final Context context
			, String userId , final HttpResponseInterface responseInterface , boolean isShowDialog){
		if(CommonUtils.isInternetConnectionAvailable(context)) {
			GetResourceVideoInAppStatusParam param = new GetResourceVideoInAppStatusParam();
			param.setAction("getVideoInAppStatus");
			param.setUserId(userId);
			param.setAppId(CommonUtils.APP_ID);
			new MyAsyckTask(ServerOperation.CreatePostRequestForGetResourcesVedioInAppStatus(param)
					, null, ServerOperationUtil.GET_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST, context,
					new HttpResponseInterface() {
				@Override
				public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
					GetResourceVideoInAppResult response =
							(GetResourceVideoInAppResult) httpResponseBase;
					if(response.getResult().equalsIgnoreCase
							(MathFriendzyHelper.SUCCESS)){
						boolean status = false;
						boolean unlockCategoryStatus = false;
						if(response.getStatus() == MathFriendzyHelper.YES){
							status = true;
						}
						if(response.getUnlockCategoryStatus() == MathFriendzyHelper.YES){
							unlockCategoryStatus = true;
						}
						MathFriendzyHelper.saveBooleanInSharedPreff(context ,
								MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY , status);
						MathFriendzyHelper.saveBooleanInSharedPreff(context ,
								MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY ,
								unlockCategoryStatus);
					}
					responseInterface.serverResponse(httpResponseBase , requestCode);
				}
			}, ServerOperationUtil.SIMPLE_DIALOG, false,
			context.getString(R.string.please_wait_dialog_msg))
			.execute();
		}else{
			//CommonUtils.showInternetDialog(context);
		}
	}


	/**
	 * Return the app unlock status
	 *
	 * @param itemId
	 * @param userId
	 * @param context
	 * @return
	 */
	public static int getAppUnlockStatus(int itemId, String userId, Context context) {
		int appStatus = 0;
		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(context);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatus(itemId, userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}

	public static void showUnlockCategoryDialogWhenUserHaveMaxCoins(final Context context
			, final OnPurchaseDone purchaseListener
			, final GetAppUnlockStatusResponse appUnlockResponse){
		String[] textArray = MathFriendzyHelper.getTreanslationTextById(context ,
				"lblUnlockAllCategoriesWithCoins" ,
				"btnTitleNoThanks" , "lblUpgrade");
		MathFriendzyHelper.yesNoConfirmationDialog(context , textArray[0] ,
				textArray[2] , textArray[1] , new YesNoListenerInterface() {
			@Override
			public void onYes() {
				MathFriendzyHelper.updateCategoryPurchasedStatus(context,
						MathFriendzyHelper.getUserId(context), new HttpResponseInterface() {
					@Override
					public void serverResponse(HttpResponseBase httpResponseBase,
							int requestCode) {

					}
				}, MathFriendzyHelper.YES);
				MathFriendzyHelper.updateUserCoins(context ,
						MathFriendzyHelper.getUserId(context) , appUnlockResponse.getCoins()
						- MAX_USER_COINS_TO_UNLOCK_APP_CATEGORY ,
						false , new HttpResponseInterface() {
					@Override
					public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

					}
				});
				purchaseListener.onPurchaseDone(true);
			}

			@Override
			public void onNo() {
				purchaseListener.onPurchaseDone(false);
			}
		});
	}

	public static String[] getTreanslationTextById(Context context , String ...args){
		String[] textArray = new String[args.length];
		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		for(int i = 0 ; i < args.length ; i ++ ) {
			textArray[i] =  transeletion.getTranselationTextByTextIdentifier(args[i]);
		}
		transeletion.closeConnection();
		return textArray;
	}

	public static String getTreanslationTextById(Context context , String translationId){
		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		String text = transeletion.getTranselationTextByTextIdentifier(translationId);
		transeletion.closeConnection();
		return text;
	}

	/**
	 * Generate the yes no interface dialog
	 *
	 * @param context
	 * @param message
	 * @param yesText
	 * @param noText
	 * @param listenerInterface
	 */
	public static void yesNoConfirmationDialog(Context context, String message,
			String yesText, String noText,
			final YesNoListenerInterface listenerInterface) {
		final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.yes_no_confirmation_dialog_layout);
		dialog.show();

		Button btnNo = (Button) dialog.findViewById(R.id.btnNoThanks);
		Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
		TextView txtMsg = (TextView) dialog.findViewById(R.id.txtTimeSpentMessage);

		btnNo.setText(noText);
		btnYes.setText(yesText);
		txtMsg.setText(message);

		btnNo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				listenerInterface.onNo();
			}
		});

		btnYes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				listenerInterface.onYes();
			}
		});
	}

	public static void saveBooleanInSharedPreff(Context context , String key , boolean bValue){
		SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putBoolean(key, bValue);
		editor.commit();
	}

	/**
	 * Generate the yes no interface dialog
	 *
	 * @param context
	 * @param message
	 * @param yesText
	 * @param noText
	 * @param listenerInterface
	 */
	public static void yesNoConfirmationDialogForLongMessageForUnlockInAppPopUp(Context context,
			String message,
			String yesText, String noText,
			final YesNoListenerInterface listenerInterface
			, YesNoDialogMessagesForInAppPopUp messages) {
		final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.yes_no_confirmation_dialog_for_in_app_purchase);
		dialog.show();

		Button btnNo = (Button) dialog.findViewById(R.id.btnNoThanks);
		Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
		TextView txtMsg = (TextView) dialog.findViewById(R.id.txtTimeSpentMessage);
		TextView txtLimitedTimeOffer = (TextView) dialog.findViewById(R.id.txtLimitedTimeOffer);
		TextView txtUpgradeAmount = (TextView) dialog.findViewById(R.id.txtUpgradeAmount);

		btnNo.setText(noText);
		btnYes.setText(yesText);
		txtMsg.setText(messages.getMainText());
		txtLimitedTimeOffer.setText("\n" + messages.getLimitedTimeOffer());
		txtUpgradeAmount.setText(messages.getUpgradeText());

		btnNo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				listenerInterface.onNo();
			}
		});

		btnYes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				listenerInterface.onYes();
			}
		});
	}

	private static void openGetMoreCoinsScreenForUnlockCategory(Context context){
		MathFriendzyHelper.openInAppScreen(context,
				GetMoreCoins.UNLOCK_MATH_APP_CATEGORY_PURCHASED,
				GetMoreCoins.START_GET_MORE_COIN_ACTIVITY_REQUEST);
	}

	public static void openInAppScreen(Context context ,
			int requestFor , int requestCode){
		if(CommonUtils.isInternetConnectionAvailable(context)) {
			Intent intent = new Intent(context, GetMoreCoins.class);
			intent.putExtra(GetMoreCoins.IN_APP_REQUEST, requestFor);
			((Activity) context).startActivityForResult(intent, requestCode);
		}else{
			CommonUtils.showInternetDialog(context);
		}
	}

	public static void updateCategoryPurchasedStatus(Context context
			, String userId
			,final HttpResponseInterface responseInterface , int spentCoin){
		MathFriendzyHelper.saveBooleanInSharedPreff(context ,
				MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY , true);
		MathFriendzyHelper.saveIsAdDisble(context , 1);
		if(CommonUtils.isInternetConnectionAvailable(context)) {
			GetResourceVideoInAppStatusParam param = new GetResourceVideoInAppStatusParam();
			param.setAction("saveUnlockCategoriesInAppStatus");
			param.setUserId(userId);
			param.setSpentCoins(spentCoin);
			param.setAppId(CommonUtils.APP_ID);
			new MyAsyckTask(ServerOperation.CreatePostRequestForGetResourcesVedioInAppStatus(param)
					, null, ServerOperationUtil.SAVE_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST, context,
					new HttpResponseInterface() {
				@Override
				public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
					responseInterface.serverResponse(httpResponseBase , requestCode);
				}
			}, ServerOperationUtil.SIMPLE_DIALOG, false,
			context.getString(R.string.please_wait_dialog_msg))
			.execute();
		}else{
			CommonUtils.showInternetDialog(context);
		}
	}

	public static void updateUserCoins(Context context , String userId , int coins ,
			boolean isShowDialog ,
			final HttpResponseInterface responseInterface){
		UserRegistrationOperation userRegOpr = new UserRegistrationOperation(context);
		userRegOpr.updaUserCoins(coins, userId);
		userRegOpr.closeConn();

		UpdateUserCoinsParam param = new UpdateUserCoinsParam();
		param.setAction("saveCoinsForUser");
		param.setUserId(userId);
		param.setCoins(coins);

		if(CommonUtils.isInternetConnectionAvailable(context)) {
			new MyAsyckTask(ServerOperation.CreatePostRequestForUpdateUserCoins(param)
					, null, ServerOperationUtil.UPDATE_USER_COINS_ON_SERVER, context,
					new HttpResponseInterface() {
				@Override
				public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
					responseInterface.serverResponse(httpResponseBase, requestCode);
				}
			}, ServerOperationUtil.SIMPLE_DIALOG, isShowDialog,
			context.getString(R.string.please_wait_dialog_msg))
			.execute();
		}
	}

	public static void saveVideoInAppStatus(final Context context, String userId ,
			final HttpResponseInterface responseInterface){
		MathFriendzyHelper.saveBooleanInSharedPreff(context ,
				MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY , true);
		if(CommonUtils.isInternetConnectionAvailable(context)) {
			GetResourceVideoInAppStatusParam param = new GetResourceVideoInAppStatusParam();
			param.setAction("saveVideoInAppStatus");
			param.setUserId(userId);
			param.setAppId(CommonUtils.APP_ID);
			new MyAsyckTask(ServerOperation.CreatePostRequestForGetResourcesVedioInAppStatus(param)
					, null, ServerOperationUtil.SAVE_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST, context,
					new HttpResponseInterface() {
				@Override
				public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
					responseInterface.serverResponse(httpResponseBase , requestCode);
				}
			}, ServerOperationUtil.SIMPLE_DIALOG, false,
			context.getString(R.string.please_wait_dialog_msg))
			.execute();
		}else{
			CommonUtils.showInternetDialog(context);
		}
	}	

	public static boolean isNeedToGetPurchaseStatus(Context context){
		boolean isVideoPurchase = MathFriendzyHelper.getBooleanValueFromPreff(context ,
				MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY);
		boolean isUnlockCategory = MathFriendzyHelper.getBooleanValueFromPreff(context ,
				MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY);
		if(!(isVideoPurchase && isUnlockCategory))
			return true;
		return false;
	}

	public static String getLastInitialName(String lastName){
		try{
			if(!MathFriendzyHelper.isEmpty(lastName)){
				return lastName.substring(0, 1);
			}
			return "";
		}catch(Exception e){
			return "";
		}
	}

	public static void setEditTextWatcherToEditTextForLastInitialName
	(final Context context , final EditText edtText){
		final String message = MathFriendzyHelper.getTreanslationTextById(context ,
				"lblOnlyLastInitial");
		edtText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String text = String.valueOf(s);
				if(text.length() > 1){
					edtText.setText(MathFriendzyHelper.getLastInitialName(text));
					MathFriendzyHelper.showWarningDialog(context , message);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
	}

	public static String MATH_TUTOR_PLUS_LINK 
	= "https://play.google.com/store/apps/details?id=com.math_tutor_plus";
	public static final String PROFESSIONAL_TUTORING_WATCH_VEDIO_URL =
			"https://www.youtube.com/watch?v=H90FAVFsSMM";
	public static final String PROFESSIONAL_TUTORING_OVERVIEW = 
			"https://www.youtube.com/watch?v=z0Oa0qt6cqs";
	//for youtube vedio change format to embad
	private static String REPLACE_TEXT = "watch?v=";
	private static String REPLACE_BY_TEXT = "embed/";
	public static void openUrl(Context context , String url){		
		try{
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			context.startActivity(intent);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Play youtube vedios into embadded format vedios
	 *
	 * @param context
	 * @param url
	 */
	public static void converUrlIntoEmbaddedAndPlay(Context context, String url) {
		try {			
			if(url.contains("&")){
				url = url.substring(0 , url.indexOf("&"));
			}			
			url = url.replace(REPLACE_TEXT, REPLACE_BY_TEXT);
			url = url + "?rel=0&controls=0&showinfo=0&modestbranding=1&frameborder=0&allowfullscreen";						            
			Intent intent = new Intent(context, ActYouTubePlaye.class);
			intent.putExtra("url", url);
			context.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send Email Using email intent
	 * @param context
	 * @param receiver
	 * @param subject
	 * @param body
	 * @param createChooserMsh
	 */
	public static void sendEmailUsingEmailChooser(Context context , String receiver ,
			String subject , String body ,
			String createChooserMsh){
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
				"mailto", receiver, null));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT,subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, body);
		context.startActivity(Intent.createChooser(emailIntent, createChooserMsh));
	}

	private static final String RESOURCE_CATEGORIES_LIST = "com.mathfriendzy_resource_categories";
	public static ResourceCategory getSavedResourceCategories(Context context){
		try {
			SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
			Gson gson = new Gson();
			String json = null;
			json = sharedPreff.getString(RESOURCE_CATEGORIES_LIST, "");
			if (MathFriendzyHelper.isEmpty(json)) {
				return null;
			}
			Type type = new TypeToken<ResourceCategory>() {
			}.getType();
			ResourceCategory resources = gson.fromJson(json, type);
			return resources;
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public static void getResourceCategories(final Context context ,
			final HttpResponseInterface responseInterface){
		ResourceCategory resourceCategory = getSavedResourceCategories(context);
		//String date = "";
		if(resourceCategory != null){
			DateObj dateObj = CommonUtils.getDateTimeDiff(new Date() ,
					MathFriendzyHelper.getValidateDate(resourceCategory.getDate(),
							"yyyy-MM-dd HH:mm:ss"));
			//date = resourceCategory.getDate();
			if(!(dateObj.getDiffInDay() >= 7)){
				responseInterface.serverResponse(resourceCategory ,
						ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST);
				return ;
			}
		}

		if(CommonUtils.isInternetConnectionAvailable(context)){
			GetResourceCategoriesParam param = new GetResourceCategoriesParam();
			param.setAction("getGooruCategories");
			param.setDate("");//get the fresh record when date is blank
			new MyAsyckTask(ServerOperation.CreatePostRequestForGetResourcesCategories(param)
					, null, ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST, context,
					new HttpResponseInterface() {
				@Override
				public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
					ResourceCategory resourceCategory = (ResourceCategory) httpResponseBase;
					if(resourceCategory.getResult()
							.equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
						if(resourceCategory.getCategoriesList().size() > 0) {
							saveResourceCategories(context, resourceCategory);
						}
					}
					responseInterface.serverResponse(httpResponseBase, requestCode);
				}
			}, ServerOperationUtil.SIMPLE_DIALOG, true,
			context.getString(R.string.please_wait_dialog_msg))
			.execute();
		}else{
			CommonUtils.showInternetDialog(context);
		}
	}
	public static void saveResourceCategories(Context context , ResourceCategory resources){
		SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor editor = sharedPreff.edit();
		Gson gson = new Gson();
		String json = gson.toJson(resources);
		editor.putString(RESOURCE_CATEGORIES_LIST, json);
		editor.commit();
	}

	public static String replaceDoubleQuotesBySingleQuote(String stringWithDoubleQuotes){
		try{
			if(stringWithDoubleQuotes.contains("''"))
				return stringWithDoubleQuotes.replaceAll("''" ,"'");
			return stringWithDoubleQuotes;
		}catch(Exception e){
			return "";
		}
	}

	/**
	 * Return the string in the given formate
	 *
	 * @param formate
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getCurrentDateInGiveGformateDate(String formate) {
		try {
			SimpleDateFormat convertedFomate = new SimpleDateFormat(formate);
			return convertedFomate.format(new Date());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void showWarningDialog(Context context, String message,
			HttpServerRequest request) {
		DialogGenerator dg = new DialogGenerator(context);
		dg.generateCommonWarningDialog(message, request);
	}

	public static void openResourceScreen(Context context , ResourceSearchTermParam searchTerm){
		Intent intent = new Intent(context , ActResourceHome.class);
		intent.putExtra("searchParam" , searchTerm);
		context.startActivity(intent);
	}

	/**
	 * OPen url in webview
	 * @param context
	 * @param url
	 */
	public static void openUrlInWebView(Context context, String url){
		try {
			if(url.contains("youtube")){
				converUrlIntoEmbaddedAndPlay(context , url);
				return ;
			}			
			Intent intent = new Intent(context, MyWebView.class);
			intent.putExtra("url", url);
			context.startActivity(intent);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Separate the string with commas and return the string array
	 *
	 * @return
	 */
	public static String[] getCommaSepratedOption(String stringWithCommas, String delimiter) {
		try {
			return stringWithCommas.split(delimiter);
		} catch (Exception e) {
			e.printStackTrace();
			return new String[0];//return blank array
		}
	}

	/**
	 * Return the app unlock status
	 *
	 * @param itemId
	 * @param userId
	 * @param context
	 * @return
	 */
	public static int getAppUnlockStatusForReourcePurchase
	(int itemId, String userId, Context context) {
		int appStatus = 0;
		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(context);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatusForResourcePurchase(itemId, userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}	

	/**
	 * Return the App Version
	 * @param context
	 * @return
	 */
	public static String getAppVersion(Context context){
		PackageInfo pInfo = null;
		String version = "";
		try {
			pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			version = pInfo.versionName;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
	}

	/**
	 * Return the App Version
	 * @param context
	 * @return
	 */
	public static String getAppVersionTextForTesting(Context context){
		//return "00112315";//00MMDDYY
		return "";
	}

	/**
	 * Get display option
	 * @return
	 */
	public static DisplayImageOptions getDisplayOptionWithoutLoadingFail(){
		return new DisplayImageOptions.Builder()
		//.showImageOnLoading(R.drawable.resources)
		//.showImageForEmptyUri(R.drawable.resources)
		//.showImageOnFail(R.drawable.resources)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.considerExifParams(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}


	//not login user resource purchase change
	public static final String RESOURCE_PURCHASE_STATUS_KEY_FOR_DEVICE
	= "com.mathfriendzy.resource.purchase.key.for.device";
	public static final String UNLOCK_CATEGORY_PURCHASE_STATUS_KEY_FOR_DEVICE
	= "com.mathfriendzy.unlock_category.purchase.key.for.device";
	public static final int OPEN_LOGIN_SCREEN_REQUEST_CODE = 1000001;
	public static final int OPEN_REGISTRATION_SCREEN_REQUEST_CODE = 1000002;
	public static boolean isAppUnlockOrResourcePurchased(Context context){
		//Save this when user make purchase without register or login
		if(MathFriendzyHelper.getBooleanValueFromSharedPreff(context ,
				MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY_FOR_DEVICE)){
			return true;
		}

		if(MathFriendzyHelper.getAppUnlockStatus(APP_UNLOCK_ID ,
				MathFriendzyHelper.getUserId(context) , context)
				== MathFriendzyHelper.APP_UNLOCK){
			return true;
		}
		//save this value when user login
		return MathFriendzyHelper.getBooleanValueFromPreff(context ,
				MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY);
	}

	/**
	 * Show login registration dialog
	 *
	 * @param context
	 */
	public static void showLoginRegistrationDialog(Context context, String msg ,
			LoginRegisterPopUpListener listener) {
		DialogGenerator dg = new DialogGenerator(context);
		dg.generateRegisterOrLogInDialog(msg , listener);
	}

	public static void openLogin(Context context , boolean isOpenForInAppPurchase , int requestCode){
		Intent intent = new Intent(context, LoginActivity.class);
		intent.putExtra("callingActivity", ((Activity)context).getClass().getSimpleName());
		intent.putExtra("isOpenForInAppPurchase", isOpenForInAppPurchase);
		((Activity)context).startActivityForResult(intent, requestCode);
	}

	public static void openRegistration(Context context , boolean isOpenForInAppPurchase , int requestCode){
		Intent intent = new Intent(context, RegistrationStep1.class);
		intent.putExtra("isOpenForInAppPurchase", isOpenForInAppPurchase);
		((Activity)context).startActivityForResult(intent , requestCode);
	}


	public static boolean isAppUnlockCategoryPurchased(Context context){
		//Save this when user make purchase without register or login
		if(MathFriendzyHelper.getBooleanValueFromSharedPreff(context ,
				MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY_FOR_DEVICE)){            
			return true;
		}

		//save this value when user login
		return MathFriendzyHelper.getBooleanValueFromPreff(context ,
				MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY);
	}

	public static void showUnlockMathCategoryDialog(final Context context , int requestFrom){
		final String[] textArray = MathFriendzyHelper.getTreanslationTextById(context , "lblUnlockAllCategories" ,
				"btnTitleNoThanks" , "btnTitleOK" , "lblLimitedTimeOffer" ,
				"lblUpgradeUnlockCategory" , "lblUpgradeMathCategoryOnly" , "lblWouldLikeToLoginBeforePurchase");
		YesNoDialogMessagesForInAppPopUp message = new YesNoDialogMessagesForInAppPopUp();
		message.setMainText(textArray[4]);
		message.setLimitedTimeOffer(textArray[3]);
		message.setUpgradeText(textArray[5]);
		MathFriendzyHelper.yesNoConfirmationDialogForLongMessageForUnlockInAppPopUp(context, textArray[0]
				, textArray[2], textArray[1], new YesNoListenerInterface() {
			@Override
			public void onYes() {
				if (!MathFriendzyHelper.isUserLogin(context)) {
					//MathFriendzyHelper.showLoginRegistrationDialog(context);
					MathFriendzyHelper.showLoginRegistrationDialog(context
							, textArray[6] , new LoginRegisterPopUpListener() {
						@Override
						public void onRegister() {
							MathFriendzyHelper.openRegistration(context , true ,
									MathFriendzyHelper.OPEN_REGISTRATION_SCREEN_REQUEST_CODE);
						}

						@Override
						public void onLogin() {
							MathFriendzyHelper.openLogin(context , true ,
									MathFriendzyHelper.OPEN_LOGIN_SCREEN_REQUEST_CODE);
						}

						@Override
						public void onNoThanks() {
							openGetMoreCoinsScreenForUnlockCategory(context);
						}
					});
					return;
				}
				openGetMoreCoinsScreenForUnlockCategory(context);
			}

			@Override
			public void onNo() {
				//nothing to do
			}
		} , message);
	}  

	public static boolean getBooleanValueFromSharedPreff(Context context , String key){
		return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
				.getBoolean(key, false);
	}   

	//Temp player change
	public static final String TEMP_PLAYER_F_NAME = "Temp Payer";
	public static final String TEMP_PLAYER_L_NAME = "Update Now";


	private static final String SELECTED_RESOURCE_LANG = "com.math.selected.resource.lang";
	public static void saveResorceSelectedLanguage(Context context , int selectedLang){
		MathFriendzyHelper.saveIntegerValue(context , SELECTED_RESOURCE_LANG , selectedLang);
	}

	public static int getResourceSelectedLanguage(Context context){
		return MathFriendzyHelper.getIntValueFromPreff(context , SELECTED_RESOURCE_LANG);
	}

	//hide the dialog
	public static void hideDialog(Dialog dialog){
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
	}

	/**
	 * Get Resource Link From khan video
	 * @param context
	 * @param link
	 * @param responseInterface
	 */
	public static void getResourceUrlFromLink(Context context , String link ,
			HttpResponseInterface responseInterface){
		if(CommonUtils.isInternetConnectionAvailable(context)){
			GetKhanVideoLinkParam param = new GetKhanVideoLinkParam();
			param.setUrl(link);
			new MyAsyckTask(ServerOperation
					.createPostRequestForGetKhanVideoLink(param)
					, null, ServerOperationUtil.GET_KHAN_VIDEO_LINK, context,
					responseInterface, ServerOperationUtil.SIMPLE_DIALOG , true ,
					context.getString(R.string.please_wait_dialog_msg))
			.execute();
		}else{
			CommonUtils.showInternetDialog(context);
		}
	}

	/**
	 * Send Email Using email intent
	 * @param context
	 * @param receiver
	 * @param subject
	 * @param body
	 * @param createChooserMsh
	 */
	public static void sendEmail(Context context , String receiver ,
			String subject , String body , String createChooserMsh){
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_EMAIL , new String[]{receiver});
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		sendIntent.putExtra(Intent.EXTRA_TEXT, body);
		sendIntent.setType("text/html");
		final PackageManager pm = context.getPackageManager();
		final List<ResolveInfo> matches = pm.queryIntentActivities(sendIntent, 0);
		ResolveInfo best = null;
		for(final ResolveInfo info : matches)
			if (info.activityInfo.packageName.endsWith(".gm")
					|| info.activityInfo.name.toLowerCase().contains("gmail"))
				best = info;
		if (best != null)
			sendIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
		context.startActivity(sendIntent);
	}

	public static String getAppName(Context context) {
		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		String appName = ITextIds.LEVEL+" "+transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE);		
		transeletion.closeConnection();
		return appName;
	}
	
	//Rate us 
	public static void rateUs(Context context){
		try{
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(ITextIds.RATE_URL));
			context.startActivity(intent);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Return the school video uri from raw folder
	 * @param context
	 * @return
	 */
	public static String getSchoolAdVideUri(Context context){
		try{
			String path = "android.resource://" + 
					context.getPackageName() + "/" + R.raw.school_advertise;
			return path;
		}catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
