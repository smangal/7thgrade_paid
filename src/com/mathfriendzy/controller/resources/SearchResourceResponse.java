package com.mathfriendzy.controller.resources;

import java.io.Serializable;
import java.util.ArrayList;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 6/8/15.
 */
public class SearchResourceResponse extends HttpResponseBase implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String result;
    private String totalHitCount;
    private ArrayList<ResourceResponse> listOfresource = new ArrayList<ResourceResponse>();
    private int totalTab;

    public int getTotalTab() {
        return totalTab;
    }

    public void setTotalTab(int totalTab) {
        this.totalTab = totalTab;
    }

    public String getTotalHitCount() {
        return totalHitCount;
    }

    public void setTotalHitCount(String totalHitCount) {
        this.totalHitCount = totalHitCount;
    }

    public ArrayList<ResourceResponse> getListOfresource() {
        return listOfresource;
    }

    public void setListOfresource(ArrayList<ResourceResponse> listOfresource) {
        this.listOfresource = listOfresource;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
