package com.mathfriendzy.controller.top100;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mathfriendzy.model.top100.TopBeans;
import com.seventhgradepaid.R;

public class ListAdapter extends BaseAdapter
{
	Context context;
	int id;
	ArrayList<TopBeans> topRecord;

	public ListAdapter(Context context, int id, ArrayList<TopBeans> topRecord)
	{
		this.context = context;
		this.id 	= id;
		this.topRecord = topRecord;
	}



	@Override
	public int getCount() 
	{
		return topRecord.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int arg0) 
	{
		return 0;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View row, ViewGroup parent)
	{
		ViewHolder holder = null;
		if (row == null)
		{
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.top100_list, parent, false);

			holder				= new ViewHolder();
			holder.name 		= (TextView) row.findViewById(R.id.txtName);
			holder.city			= (TextView) row.findViewById(R.id.txtCity);
			holder.school		= (TextView) row.findViewById(R.id.txtSchoolname);
			holder.txtSerialNo	= (TextView) row.findViewById(R.id.txtSerialNo);
			holder.txtPointsSum	= (TextView) row.findViewById(R.id.txtPointsSum);
			holder.imgCountry	= (ImageView) row.findViewById(R.id.imgcountry);			

			row.setTag(holder);
		}
		else 
		{
			holder = (ViewHolder) row.getTag();
		}
		
		holder.name.setText(""+topRecord.get(position).getName());
		
		if(topRecord.get(position).getCity().length() > 0 && topRecord.get(position).getState().length() > 0)
		{
			holder.city.setText(""+topRecord.get(position).getCity()+" , "+topRecord.get(position).getState());			
		}
		else
		{
			holder.city.setText(""+topRecord.get(position).getCity()+""+topRecord.get(position).getState());			
		}

		
		holder.txtPointsSum.setText(""+setNumberString(topRecord.get(position).getSum()));
		holder.txtSerialNo.setText(""+(position+1)+".");

		if(id == 2)
		{
			holder.school.setVisibility(View.VISIBLE);
			holder.school.setText(topRecord.get(position).getSchool());
		}


		String countryName = topRecord.get(position).getCountry();
		if(countryName != null)
		{
			InputStream ins;
			String folder = context.getResources().getString(R.string.countryImageFolder);
			String imgUrl = folder+"/"+countryName+".png";
			try 
			{
				ins = context.getAssets().open(imgUrl);
				Drawable drawable 	= Drawable.createFromStream(ins, null);
				holder.imgCountry.setBackgroundDrawable(drawable);  
			} catch (IOException e) 
			{
				holder.imgCountry.setBackgroundResource(0);
			}
		} 

		return row;
	}


	private String setNumberString(String str)
	{
		double amount = Double.parseDouble(str);
		DecimalFormat formatter = new DecimalFormat("#,##,##,###");
		return formatter.format(amount);
	}


	class ViewHolder
	{
		private TextView txtSerialNo;
		private TextView txtPointsSum;
		private TextView name;
		private TextView city;
		private TextView school;
		private ImageView imgCountry;

	}
}
