package com.mathfriendzy.controller.youtubeplayer;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.utils.CommonUtils;
import com.seventhgradepaid.R;

public class ActYouTubePlaye extends AdBaseActivity {

	private WebView webView = null;
	private ProgressBar progressBar = null;

	private final String TAG = this.getClass().getSimpleName();
	private String url = null;
	private final String REPLACE_TEXT = "&feature=player_embedded";
	private final String REPLACE_TEXT1= "&list=PLB2606C762CF0B898&index=41&feature=plpp_video";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_you_tube_playe);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

		this.getIntentValues();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setWebViewSetting();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");
	}

	private void getIntentValues(){
		url = getIntent().getStringExtra("url").replaceAll(REPLACE_TEXT , "")
                .replaceAll(REPLACE_TEXT1 , "");			
		CommonUtils.printLog(TAG, "url " + url);
	}


	protected void setWidgetsReferences() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setWidgetsReferences()");

		webView = (WebView) findViewById(R.id.webView);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setWidgetsReferences()");
	}



	protected void setListenerOnWidgets() {

	}



	protected void setTextFromTranslation() {

	}

	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	private void setWebViewSetting(){

		WebSettings webSettings = webView.getSettings();
		webSettings.setAppCacheEnabled(false);		
		webView.setInitialScale(1);
		webSettings.setPluginState(PluginState.ON);		
		webSettings.setJavaScriptEnabled(true);
		webSettings.setBuiltInZoomControls(false);
		webSettings.setAllowContentAccess(true);
		webSettings.setEnableSmoothTransition(true);
		webSettings.setLoadsImagesAutomatically(true);
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setSupportZoom(true);
		webSettings.setUseWideViewPort(true);
		webView.setWebViewClient(new MyWebViewClient());
		//webView.setWebChromeClient(new MyWebChromeClient());			
	}
	
	//load url from onResume
	@Override
	protected void onResume() {
		webView.loadUrl(url);
		super.onResume();
	}

	/**
	 * Load blank page to the webview
	 */
	private void loadBlankPage(){
		webView.loadUrl("about:blank");
	}

	//load blank url to stop the previous one
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.loadBlankPage();
	}

	//load blank url to stop the previous one
	@Override
	protected void onStop() {
		super.onStop();
		this.loadBlankPage();
	}

	class MyWebViewClient extends WebViewClient{

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			progressBar.setVisibility(ProgressBar.VISIBLE);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			progressBar.setVisibility(ProgressBar.GONE);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);
			progressBar.setVisibility(ProgressBar.GONE);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}
	
	class MyWebChromeClient extends WebChromeClient{
		
	}
}
