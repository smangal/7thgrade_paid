package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_USER_PLAYER_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.learningcenter.LearningCenterMain;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.customview.DynamicLayout;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ITextIds;
import com.seventhgradepaid.R;

/**
 * This shows the no. of player exits under the login user
 * @author Yashwant Singh
 *
 */
public class LoginUserPlayerActivity extends AdBaseActivity implements OnClickListener
{
	private TextView mfTitleHomeScreen 		= null;
	private TextView lblPleaseSelectPlayer 	= null;
	private TextView textResult  			= null;
	private TextView btnTitleEdit 			= null;
	private Button   lblAddPlayer 			= null;
	private Button   playTitle 				= null;
	private Button   btnTitleTop100         = null;
	private TextView btnTitlePlayers        = null;
	//private ListView playerListView 		= null;
	private ArrayList<UserPlayerDto> userPlayerList   = null;
	private LinearLayout linearLayout = null;
	
	private final String TAG = this.getClass().getSimpleName();
	private Button   btnTitleBack      = null;
	
	private final int MAX_ITEM_ID = 11;// if item id is 11 the unlock the level
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_user_player);
		
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside onCreate()");
			
		this.setWidgetsReferences();
		this.setWidgetsTextValue();
		this.getUserPlayersData();
		this.setListenerOnWidgets();
		
		/*UserPlayersAdapter adapter = new UserPlayersAdapter(this,R.layout.list_players,userPlayerList);
		playerListView.setAdapter(adapter);*/
		//this.createDyanamicLayoutForDisplayUserPlayer();
		
		DynamicLayout dynamicLayout = new DynamicLayout(this);
		dynamicLayout.createDyanamicLayoutForDisplayUserPlayer(userPlayerList, linearLayout, false);
		
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside onCreate()");
	}
	
	/**
	 * This method set the references from the layout to the widgets objects
	 */
	private void setWidgetsReferences()
	{
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside setWidgetsReferences()");
		
		//playerListView 			= (ListView) findViewById(R.id.playerListView);
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblPleaseSelectPlayer 	= (TextView) findViewById(R.id.lblPleaseSelectPlayer);
		textResult 				= (TextView) findViewById(R.id.textResult);
		btnTitleEdit 			= (TextView) findViewById(R.id.btnTitleEdit);
		lblAddPlayer 			= (Button)   findViewById(R.id.lblAddPlayer);
		playTitle    			= (Button)   findViewById(R.id.playTitle);
		btnTitleTop100          = (Button)   findViewById(R.id.btnTitleTop100);
		btnTitlePlayers         = (TextView) findViewById(R.id.btnTitlePlayers);
		linearLayout            = (LinearLayout) findViewById(R.id.userPlayerLayout);
		btnTitleBack            = (Button)   findViewById(R.id.btnTitleBack);
		
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside setWidgetsReferences()");
	}
	
	/**
	 * this method set the text Widgets text values from the translation
	 */
	private void setWidgetsTextValue()
	{
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside setWidgetsTextValue()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(ITextIds.LEVEL+" "+transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE));
		lblPleaseSelectPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAPlayer"));
		textResult.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleResults"));
		btnTitleEdit.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleEdit"));
		lblAddPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayer"));
		playTitle.setText(transeletion.getTranselationTextByTextIdentifier("playTitle")
		          + " " + transeletion.getTranselationTextByTextIdentifier("lblNow") + "!");
		btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
		btnTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePlayers"));
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		transeletion.closeConnection();
		
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside setWidgetsTextValue()");
	}
	
	/**
	 * this method get player data from the database
	 */
	private void getUserPlayersData() 
	{
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside getUserPlayersData()");
		
		UserPlayerOperation userPlayerObj = new UserPlayerOperation(this);
		userPlayerList = userPlayerObj.getUserPlayerData();
			
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside getUserPlayersData()");
	}
	
	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets()
	{
		lblAddPlayer.setOnClickListener(this);
		playTitle.setOnClickListener(this);
		btnTitleTop100.setOnClickListener(this);
		btnTitleBack.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.lblAddPlayer:
			startActivity(new Intent(this,AddPlayer.class).putExtra("callingActivity", "LoginUserPlayerActivity"));
			break;
		case R.id.playTitle:
			this.clickOnPlayTitle();
			break;
		case R.id.btnTitleBack:
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			break;
		case R.id.btnTitleTop100 :
			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
			   startActivity(new Intent(this,Top100Activity.class));
			}
			break;
		}
		
	}
	
	
	/**
	 * This method call when user click on play now button
	 */
	private void clickOnPlayTitle()
	{
		if(MainActivity.IS_CALL_FROM_LEARNING_CENTER == 1)
		{
			SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 
			
			if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("lblPleaseSelectPlayer"));
				transeletion.closeConnection();
			}
			else
			{
				MainActivity.IS_CALL_FROM_LEARNING_CENTER = 0;
				SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
				SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();
				
				UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
				UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));
				
				UserRegistrationOperation userObj = new UserRegistrationOperation(this);
				RegistereUserDto regUserObj = userObj.getUserData();
									
				Intent intent = new Intent(this,LearningCenterMain.class);
									
				editor.clear();
				/*editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
				editor.putString("city", regUserObj.getCity());
				editor.putString("state", regUserObj.getState());
				editor.putString("imageName",  userPlayerData.getImageName());
				editor.putString("coins",  userPlayerData.getCoin());
				editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));*/
				
				editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
				editor.putString("city", regUserObj.getCity());
				editor.putString("state", regUserObj.getState());
				editor.putString("imageName",  userPlayerData.getImageName());
				editor.putString("coins",  userPlayerData.getCoin());
				editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
				editor.putString("userId",  userPlayerData.getParentUserId());
				editor.putString("playerId",  userPlayerData.getPlayerid());
				Country country = new Country();
				editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
				editor.commit();
				
				startActivity(intent);
			}
			
		}
		else if(MainActivity.IS_CALL_FROM_SINGLE_FRIENDZY == 1)
		{
			SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 
			
			if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("lblPleaseSelectPlayer"));
				transeletion.closeConnection();
			}
			else
			{
				MainActivity.IS_CALL_FROM_SINGLE_FRIENDZY = 0;
				UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
				UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));
				
				
					SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(this);
					singleimpl.openConn();
					int itemId = singleimpl.getItemId(userPlayerData.getParentUserId());
					singleimpl.closeConn();
					
					MainActivity x = new MainActivity();
					
					if(itemId != MAX_ITEM_ID)
					{
						if(CommonUtils.isInternetConnectionAvailable(this))
						{
							x.new GetSingleFriendzyDetail(userPlayerData.getParentUserId() , userPlayerData.getPlayerid()
									,this).execute(null,null,null);
						}
						else
						{
							/*DialogGenerator dg = new DialogGenerator(this);
							Translation transeletion = new Translation(this);
							transeletion.openConnection();
							dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
							transeletion.closeConnection();*/
							setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity();
						}
					}
					else
					{
						if(CommonUtils.isInternetConnectionAvailable(this))
						{
							x.new GetpointsLevelDetail(userPlayerData.getParentUserId(), 
									userPlayerData.getPlayerid() , this).execute(null,null,null);
						}
						else
						{
							/*DialogGenerator dg = new DialogGenerator(this);
							Translation transeletion = new Translation(this);
							transeletion.openConnection();
							dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
							transeletion.closeConnection();*/
							setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity();
						}
											
					}
							
			}
		}
		else if(MainActivity.IS_CALL_FROM_MULTIFRIENDZY == 1)
		{
			SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 
			
			if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("lblPleaseSelectPlayer"));
				transeletion.closeConnection();
			}
			else
			{
				MainActivity.IS_CALL_FROM_MULTIFRIENDZY = 0;
				SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
				SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();
				
				UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
				UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));
				
				UserRegistrationOperation userObj = new UserRegistrationOperation(this);
				RegistereUserDto regUserObj = userObj.getUserData();
									
				Intent intent = new Intent(this,MultiFriendzyMain.class);
									
				editor.clear();
							
				editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
				editor.putString("city", regUserObj.getCity());
				editor.putString("state", regUserObj.getState());
				editor.putString("imageName",  userPlayerData.getImageName());
				editor.putString("coins",  userPlayerData.getCoin());
				editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
				editor.putString("userId",  userPlayerData.getParentUserId());
				editor.putString("playerId",  userPlayerData.getPlayerid());
				Country country = new Country();
				editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
				editor.commit();
				
				startActivity(intent);
			}
		}
		else
		{
			Intent intentMain1 = new Intent(this,MainActivity.class);
			startActivity(intentMain1);
		}
	}
	
	/**
	 * This method call after the data from server is loaded for single friendzy
	 */
	private void setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity()
	{
		SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
		UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
		UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));
		
		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();
		
			UserRegistrationOperation userObj = new UserRegistrationOperation(this);
			RegistereUserDto regUserObj = userObj.getUserData();
								
			Intent intent = new Intent(this,SingleFriendzyMain.class);
								
			editor.clear();
			editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
			editor.putString("city", regUserObj.getCity());
			editor.putString("state", regUserObj.getState());
			editor.putString("imageName",  userPlayerData.getImageName());
			editor.putString("coins",  userPlayerData.getCoin());
			editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
			editor.putString("userId",  userPlayerData.getParentUserId());
			editor.putString("playerId",  userPlayerData.getPlayerid());
			Country country = new Country();
			editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
				
			LearningCenterimpl learningCenter = new LearningCenterimpl(this);
			learningCenter.openConn();
			
			if(learningCenter.getDataFromPlayerTotalPoints(userPlayerData.getPlayerid())
											.getCompleteLevel() != 0)
			editor.putInt("completeLevel", learningCenter.getDataFromPlayerTotalPoints(userPlayerData.getPlayerid())
											.getCompleteLevel());
			else
				editor.putInt("completeLevel", Integer.parseInt(userPlayerData.getCompletelavel()));
						
			learningCenter.closeConn();
			//for score update
			editor.commit();
			
			startActivity(intent);
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
}
