package com.mathfriendzy.controller.school;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.seventhgradepaid.R;

public class SimpleListAdapter extends BaseAdapter
{
	Context context;
	ArrayList<String> nameList;
	boolean[] sepAtIndex;
	String name;
	int textSize;
	
	
	public SimpleListAdapter(Context context, ArrayList<String> schoolOrTeacherList, int TextSize)
	{
		this.context = context;
		nameList = schoolOrTeacherList;
		addSeprator(nameList);
		this.textSize  = TextSize;
	}

	@Override
	public int getCount() {
		return nameList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View row, ViewGroup parent)
	{
		ViewHolder holder = null;
		if (row == null)
		{
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.school_list, parent, false);					
			
			holder = new ViewHolder();
			holder.layout  = (LinearLayout) row.findViewById(R.id.titleLayout);
			holder.title   = (TextView) row.findViewById(R.id.title);
			holder.txtName = (TextView) row.findViewById(R.id.txtName);
			row.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) row.getTag();
		}
		holder.txtName.setTextSize(textSize);
		
		if(nameList.size() != 0)
		{
			name = nameList.get(position);
			if ( sepAtIndex[position] )
			{					
				holder.populateView("  "+name.charAt(0), true);
			}
			else
			{
				holder.populateView(" ", false);
			}
			
		}//ENd if part of upcoming button	
		
		return row;
	}

	/**
	 * uses to add seprator of months, week and tommorow
	 * @param items list of ContactDetail
	 */
	private void addSeprator(ArrayList<String> items)
	{
		sepAtIndex = new boolean[items.size()];
		char ch = '0';

		for(int i = 0; i<items.size(); i++)
		{			
			if(items.get(i).charAt(0) != ch)
			{
				sepAtIndex[i] = true;
			} else
			{
				sepAtIndex[i] = false;
			}

			ch = items.get(i).charAt(0);
		}
	}

	
	private class ViewHolder
	{
		TextView txtName;
		TextView title;
		LinearLayout layout;

		public void populateView(String value, boolean needSeparator) 
		{
			if (needSeparator) 		
			{
				title.setText(value);
				layout.setVisibility(View.VISIBLE);
			} else 
			{
				layout.setVisibility(View.GONE);
			}
			txtName.setText(""+name);
			
		}//END populateView method

	}//END viewholder class	
	
	
}
