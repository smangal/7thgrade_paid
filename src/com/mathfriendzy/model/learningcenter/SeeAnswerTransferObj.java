package com.mathfriendzy.model.learningcenter;

import java.util.ArrayList;

public class SeeAnswerTransferObj 
{
	private String playerName;
	private int noOfCorrectAnswer;
	private int level;
	private int points;
	private String countryISO;
	private ArrayList<LearningCenterTransferObj> equationList;
	private ArrayList<String> userAnswerList;
	
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public int getNoOfCorrectAnswer() {
		return noOfCorrectAnswer;
	}
	public void setNoOfCorrectAnswer(int noOfCorrectAnswer) {
		this.noOfCorrectAnswer = noOfCorrectAnswer;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public String getCountryISO() {
		return countryISO;
	}
	public void setCountryISO(String countryISO) {
		this.countryISO = countryISO;
	}
	public ArrayList<LearningCenterTransferObj> getEquationList() {
		return equationList;
	}
	public void setEquationList(ArrayList<LearningCenterTransferObj> equationList) {
		this.equationList = equationList;
	}
	public ArrayList<String> getUserAnswerList() {
		return userAnswerList;
	}
	public void setUserAnswerList(ArrayList<String> userAnswerList) {
		this.userAnswerList = userAnswerList;
	}
	
}
